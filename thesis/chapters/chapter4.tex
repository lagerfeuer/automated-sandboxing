\section{Implementation}\label{sec:implementation}
In this section, we describe how \texttt{auto-seccomp} is implemented.
Section~\ref{ssec:choosing-interface} explains which Clang interface is best
suited for \texttt{auto-seccomp}.
Section~\ref{ssec:syscall-detection} illustrates how syscall detection works in
regards to libTooling and the AST.
Section~\ref{ssec:seccomp-generation} shows how the final seccomp filter list is
generated.
Section~\ref{ssec:refactoring} shows the changes \texttt{auto-seccomp} applies
to the source code.
Section~\ref{ssec:impl-clang-llvm} provides general information about the Clang
and LLVM bindings.
Section~\ref{ssec:final-auto-seccomp} presents the final version of
\texttt{auto-seccomp} and the extended class diagram.

\subsection{Choosing an interface}\label{ssec:choosing-interface}
This tool is written in C++, eliminating libClang as a possible option.
% Since recognized syscalls are kept in a list across files, and the generated
% filters will be dumped to a new source file after the last file was processed,
Since \texttt{auto-seccomp} keeps a list of recognized syscalls in a permanent
list across files and dumps the generated filters to a new source file after the
last input file was processed,
libTooling emerges as the preferred choice.

\subsubsection{LibTooling}
LibTooling requires some boilerplate code to get started.
Clang provides many classes that interact with each other.
Creating a tool consists of developing custom classes which inherit from the
relevant Clang classes and selectively overriding the needed methods, which
perform actions on AST nodes (further explained in
Section~\ref{ssec:syscall-detection}).

\begin{sloppypar}
  A \texttt{clang::tooling::ClangTool} will run a
  \texttt{clang::tooling::ToolAction} when invoked. The class
  \texttt{clang::tooling::FrontendActionFactory} inherits from %\linebreak
  \texttt{clang::tooling::ToolAction} and will create
  \texttt{clang::FrontendActions} for every new Translation Unit
  \cite{noauthor_how_nodate}.
  This can be seen in Appendix~\ref{appendix:clang-tool}.
  Additionally, an \emph{OptionCategory} can be defined, to print custom help
  messages and support custom flags passed to the tool
  \cite{noauthor_clang::tooling::commonoptionsparser_nodate}.
\end{sloppypar}

Figure~\ref{fig:uml-seccomp-tool} shows the class hierarchy of
\texttt{auto-seccomp}.

\begin{figure}[p]
  \centering
  \includegraphics[width=\linewidth,keepaspectratio]{figures/ClangTool.png}
  \caption[Class diagram for base \texttt{auto-seccomp}]{
    Class diagram for a simple Clang tool (the basis of \texttt{auto-seccomp}).
    This diagram is extended in Section~\ref{sec:implementation},
    Figure~\ref{fig:uml-seccomp-tool-final} to reflect the changes needed to
    satisfy all requirements.}
  \label{fig:uml-seccomp-tool}
\end{figure}

\subsection{Syscall detection}\label{ssec:syscall-detection}
This section describes the differences in direct and indirect syscall detection,
as well as the creation of the libc function mapping.
Direct and indirect syscall detection are similar.
However, direct syscall detection is more accurate than its indirect counter
part, because indirect syscall detection is dependant on a full libc function
mapping.

\subsubsection{Direct syscall detection}\label{sssec:direct-syscall-detection}
\begin{sloppypar}
  Detecting syscalls means analyzing Clang's \emph{CallExpr}.
  For this purpose, we can override a group of methods
  starting with \texttt{Visit} in the \texttt{SeccompVisitor} class
  (see Figure~\ref{fig:uml-seccomp-tool}),
  \texttt{VisitCallExpr(clang::CallExpr* CE)} in this case.
  These methods exist for almost all node types \cite{noauthor_how_nodate}.
  As mentioned previously, nodes store a variety of attributes and information
  about the AST, which can be queried.
  To find a syscall, we compare the name of the current \emph{CallExpr} to a list
  of syscall names.
  If there is a match, the syscall will be added to a list, awaiting further
  processing.
\end{sloppypar}

\subsubsection{Indirect syscall detection}\label{sssec:indirect-syscall-detection}
Indirect syscall detection analyzes \emph{CallExpr} as well.
The difference is, instead of matching the name to a list of syscall names,
\texttt{auto-seccomp} resolves the mapping of libc function names to syscalls.
The syscalls used by libc are then added to the list.

\paragraph{Libc function mapping}
The libTooling interface can be used to extract a mapping of libc functions to
used syscalls.
Extracting a mapping works as follows:
\begin{enumerate}
\item \texttt{VisitFunctionDecl}: save the name of the current function declaration
\item \texttt{VisitCallExpr}: add the name of the call expression to the
  mapping
\end{enumerate}

Clang tools do not work with source code that is incompatible with the Clang
compiler, \ie source code that cannot be compiled using Clang.
Glibc is such a project.
However, different libc implementations are compatible with Clang and can be
used as a starting point for the function mapping, such as
diet-libc~\cite{diet-libc}.
If a different implementation is used to generate an initial mapping, the
mapping is not complete and still needs manual changes.

Special care has to be put into the detection of aliases, which are frequently
used in many libc implementations.
A function \texttt{open} might be implemented as \texttt{libc\_open} and then
aliased to \texttt{open}.
For this purpose, the \emph{PPCallbacks} class offers a \texttt{MacroExpands}
method.
This method creates hooks to check for aliasing of methods.

To make \texttt{auto-seccomp} usable for most projects, this mapping must be
100\% accurate.
At the core of \texttt{auto-seccomp}, the libc mapping is responsible for the
majority of correctness, because more often than not, developers use functions
like \texttt{printf} instead of the syscall \texttt{write}.

An initial list was generated, adapted and compiled into the binary.
A subset of libc functions are correctly mapped to syscalls and generate
correct filters.
This list is considered a work in progress and therefore unstable.
For further explanation on the limitations, see
Section~\ref{sssec:libc-mapping}.
\clearpage

\subsection{Generating seccomp filters}\label{ssec:seccomp-generation}
Once the list of used syscalls is finalized and the last file was
processed, the seccomp filters need to be generated.
Listing~\ref{lst:seccomp-example} shows the structure of the filters.
Three parts are necessary for a valid seccomp configuration:
\begin{enumerate}
\item Default action (what happens if a forbidden syscall occurs)
\item Seccomp rules (the list of allowed syscalls)
\item Load instruction (the kernel needs to know about this new rule set)
\end{enumerate}

In the end, all parts are concatenated, surrounded with a function body and
written to a file.
From here on \texttt{ctx} will be used as variable name for the seccomp
structure \texttt{scmp\_filter\_ctx}, the seccomp filter context.


\paragraph{Default action}
The application should be terminated if an illegal action is performed.
This means the seccomp filter set is initialized with
\mintinline{c}{SCMP_ACT_KILL}.
Therefore, the first line is:
\mintinline{c}{seccomp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL)}.
This can be seen in Listing~\ref{lst:seccomp-gen}, line 5.

Other possible default actions are:
\begin{itemize}
\item \mintinline{c}{SCMP_ACT_TRAP}: the program may catch the signal if a
  forbidden syscall is called (\texttt{KILL} cannot catch the signal).
\item \mintinline{c}{SCMP_ACT_ERRNO(uint16_t errno)}: calling a forbidden
  syscalls will return \texttt{errno}.
\item \mintinline{c}{SCMP_ACT_ALLOW}: the filter list will have no effect,
  everything is allowed.
\end{itemize}

\paragraph{Seccomp rules}
Without support for custom argument filters for the syscalls, the rules follow a
basic scheme:
\begin{quote}
  \mintinline{c}{seccomp_rule_add(ctx, SCMP_ACT_ALLOW, <syscall_name>, 0);}
\end{quote}
where \texttt{<syscall\_name>} is replaced with the actual syscall name,
like \texttt{write} or \texttt{mmap}.
This instruction is added for every syscall in the list, effectively
white-listing all syscalls used by the application.
Listing~\ref{lst:seccomp-gen}, lines 6 -- 8, illustrates the underlying structure.

\paragraph{Load instruction}
Finally, the seccomp filter list (now residing in memory) needs to be loaded
into the kernel.
This does not happen automatically and requires a call to
\mintinline{c}{seccomp_load(ctx)} (see Listing~\ref{lst:seccomp-gen}, line 9).

Listing~\ref{lst:seccomp-gen} shows the final code.
This code is written to a new file and included in the build process.
Source code refactoring puts a call to this function inside the
\texttt{main} function (as the first statement).
Therefore, setting up seccomp-mode before handing over execution to the original
application code.

\begin{listing}
  \inputminted[linenos,frame=lines]{c}{code/seccomp-wrapper.c}
  \caption[Seccomp wrapper function]{Wrapper function containing all the seccomp
    specific calls to put the application into seccomp-mode (for N syscalls,
    where \texttt{<syscall\_name\_1>} is the first syscall in the list and so
    on).
    The include for \texttt{auto-seccomp.h} contains the definition for
    \texttt{void auto\_seccomp\_setup()}.}\label{lst:seccomp-gen}
\end{listing}

\subsection{Refactoring}\label{ssec:refactoring}
Clang provides support for code refactoring (see
\texttt{clang-format}~\cite{noauthor_clang::clang-format_nodate}).
Using \emph{Rewriter} and \emph{SourceLocations}, every point in the source
code is represented and can be manipulated (changed, deleted or additional code
injected).
To minimize manual intervention by a developer,
\texttt{auto-seccomp} adds the include directive and rewrites the \texttt{main}
function to call the seccomp wrapper function as the first statement.

\begin{listing}
  \centering
  \inputminted[linenos,frame=lines,highlightlines={3,7},highlightcolor=hlcolor]{c}{code/main-refactor.c}
  \caption[Code refactoring - main function]{\texttt{Auto-seccomp} injectes code
    at two points: an additional include and a call to the seccomp wrapper
    function.}\label{lst:refactored-main}
\end{listing}

\paragraph{Include directive}
Clang offers a dedicated interface for preprocessor-related actions, called
\emph{PPCallbacks}.
This class provides hooks for all preprocessor directives,
such as \texttt{InclusionDirective}.
This method will be called for every inclusion directive in the source file.
Once the main file is identified,
the insert location for the new \texttt{auto-seccomp.h} is the
\emph{SourceLocation} after the last include.
Finally, the callback needs to be registered with the Clang preprocessor, using
the \texttt{addPPCallbacks} method. This must happen before Clang starts
processing the file.
While processing the includes, the tool tracks the file names to guarantee
\texttt{auto-seccomp.h} only occurs once.

\paragraph{Setup function} As seen in Figure~\ref{fig:uml-seccomp-tool}, the
various \texttt{Visit} methods are used to implement source code analysis.
A particular method, \texttt{VisitFunctionDecl}, can be used to
refactor the \texttt{main} function.
Since \texttt{main} is a \emph{FunctionDecl}, a conditional to check
(either \texttt{isMain()} provided by \emph{FunctionDecl}, or manually)
determines whether the current \emph{FunctionDecl} is \texttt{main}
\cite{noauthor_clang::functiondecl_nodate}.
Once identified, the \emph{SourceLocation} of the opening bracket serves as
injection point for the call.
An additional check is performed beforehand to determine whether the
call is already present.

Listing~\ref{lst:refactored-main} shows how the main file is changed after
\texttt{auto-seccomp} has analyzed the source code and written the filters
to \texttt{auto-seccomp.c}.
A generic makefile will automatically compile the new source file.
The only manual intervention needed by a developer is to add the
\texttt{-lseccomp} linker flag.


\subsection{Clang and LLVM}\label{ssec:impl-clang-llvm}
In order to correctly build and run \texttt{auto-seccomp}, the correct Clang and
LLVM version numbers need to be respected. Additionally, Clang tools need to
know about the compiler flags, which \texttt{auto-seccomp} handles via
\texttt{compile\_commands.json}.

\paragraph{Version}
\texttt{Auto-seccomp} was built using Clang and LLVM version 7.0.0.
The version number is significant since the libTooling interface is not guaranteed
to be backward compatible between versions.
It is known that major upgrades can introduce breaking changes.
Clang tools either must be kept up-to-date for version upgrades, or a
version number must be specified and used exclusively.

\paragraph{compile\_commands.json}
Clang tools utilize compiler internals for the generation of the AST.
This means, Clang tools need all the information about the build process the
compiler would have.
There are two ways to feed this information to the tool:
\begin{enumerate*}[label={\alph*)},font={\bfseries}]
\item passing compiler flags to the tool after the \texttt{--} separator
\item generating a \texttt{compile\_commands.json}.
\end{enumerate*}

Since compiler flags for different files may change, generating a
\texttt{compile\_commands.json} is the preferred option \cite{noauthor_json_nodate}.
Furthermore, it can be generated automatically by the build system.
This file contains an entry for every source file:
\begin{samepage}
\begin{verbatim}
[
    {
        "arguments": ["clang", "-c", "-o", "main.o", "main.c"],
        "directory": "/home/usr/path/to/source",
        "file": "main.c"
    }
]
\end{verbatim}
\end{samepage}

For projects using \texttt{cmake}, this file can be generated using built-in
commands:
\begin{alltt}
  cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .
\end{alltt}

Projects using \texttt{make} need to rely on another tool called
\emph{Build EAR}~\cite{github-bear}.

Clang tools automatically detect the \texttt{compile\_commands.json} and import
all flags for the source files.

\subsection{\texttt{auto-seccomp}}\label{ssec:final-auto-seccomp}
The core of \texttt{auto-seccomp} is the \emph{SyscallManager}, which is
responsible for the syscall detection and resolving the libc function mapping.
The \emph{SyscallManager} also manages the syscall list, which is later used to
create the seccomp filters.
All other \emph{Seccomp} classes are derived from Clang's tooling framework and
interact with the Clang/LLVM compiler internals.
Figure~\ref{fig:uml-seccomp-tool-final} shows the added extensions and how they
fit in with the existing libTooling base.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth,keepaspectratio]{figures/auto-seccomp.png}
  \caption[Extended class diagram]{Extended class diagram for \texttt{auto-seccomp}.}
  \label{fig:uml-seccomp-tool-final}
\end{figure}