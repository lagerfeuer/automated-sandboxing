\section{Results}\label{sec:results}
In this section, we provide two example programs for which \texttt{auto-seccomp}
generates seccomp filters.
We show that the filters for these programs are correct.
The advanced example further demonstrates how the sandbox protects a compromised
application against malicious actions by terminating the process after a
forbidden syscall.
Additionally, we provide statistics about the performance of
\texttt{auto-seccomp} on four different code bases.

\subsection{Usage examples}
We provide two examples: a basic example demonstrating the necessary steps to
incorporate \texttt{auto-seccomp} into a project and an advanced example
showcasing the protection measure after an attacker gains remote code execution.

\subsubsection{Basic example}

\begin{listing}[ht]
  \inputminted[linenos,frame=lines]{c}{code/hw.c}
  \caption{Initial Hello World program.}\label{lst:hw}
\end{listing}

Listing~\ref{lst:hw} shows a minimal example to test \texttt{auto-seccomp}.
The basic white-list includes \texttt{write} by default, but \texttt{printf}
uses more syscalls than just \texttt{write}.
This can be confirmed by running \texttt{ltrace -S ./example}, which yields:
\begin{verbatim}
  printf("Hello World!\n" <unfinished ...>
  SYS_fstat(1, 0x7ffc0aca1570)                         = 0
  SYS_brk(0)                                           = 0x55bfeba0c000
  SYS_brk(0x55bfeba2d000)                              = 0x55bfeba2d000
  SYS_write(1, "Hello World!\n", 13Hello World!
  )                       = 13
  <... printf resumed> )
\end{verbatim}

To run \texttt{auto-seccomp}, a {\sffamily compile\_commands.json} must be
generated, as described in Section~\ref{ssec:impl-clang-llvm}.

% A generic Makefile works best with \texttt{auto-seccomp}, using the
% \emph{wildcard} built-in for source files (\ie \texttt{SRC := \$(wildcard *.c)}).
% This automatically compiles \texttt{auto-seccomp.c} for a new \texttt{make}
% after \texttt{auto-seccomp} created it. Listing~\ref{lst:makefile} is an example
% of a generic Makefile.

% \begin{listing}[t]
%   \small
%   \inputminted[linenos,frame=lines]{makefile}{code/advanced-example/Makefile}
%   \caption[Example Makefile]{Example Makefile to compile a project with or
%     without seccomp (used with Listing~\ref{lst:hw} and \ref{lst:advanced-example}).}
%   \label{lst:makefile}
% \end{listing}

% The command \texttt{bear make} (\emph{Build EAR}, as mentioned in
% Section~\ref{ssec:impl-clang-llvm}) generates the {\sffamily
%   compile\_commands.json} for the tool, which contains all flags for the source
% files.

Finally, \texttt{auto-seccomp} can generate the seccomp filters.
Running \texttt{auto-seccomp main.c} generates the new files
\texttt{auto-seccomp.h} and \texttt{auto-seccomp.c} in the directory containing
\texttt{main.c}.
Like mentioned above, recompiling (using a generic Makefile) automatically
includes the seccomp filters.

\begin{listing}[h]
  \inputminted[linenos,frame=lines]{c}{code/hw-seccomp.c}
  \caption{Hello World program after \texttt{auto-seccomp} refactoring.}\label{lst:hw-seccomp}
\end{listing}

\begin{listing}[!h]
  \begin{minted}[linenos,frame=lines]{c}
    #include "auto-seccomp.h"
    #include <seccomp.h>

    void auto_seccomp_setup() {
      scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigreturn), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 0);
      seccomp_load(ctx);
      seccomp_release(ctx);
    }
  \end{minted}
  \caption{Automatically created seccomp filter rules for Listing~\ref{lst:hw}.}
  \label{lst:basic-filters}
\end{listing}

Listing~\ref{lst:hw-seccomp} shows the resulting source code after
\texttt{auto-seccomp} has refactored the initial program.

\begin{samepage}
  The typical process of generating the filters looks as folows:
  \begin{quote}
    \begin{alltt}
      bear make
      auto-seccomp main.c
      make
    \end{alltt}
  \end{quote}
\end{samepage}

% The first two steps (\texttt{bear make} and \texttt{auto-seccomp <source
%   files>}) can also be included in the Makefile.
% For example: dedicated target \texttt{compile-commands} and \texttt{seccomp}.

Listing~\ref{lst:basic-filters} illustrates the created filters and comparing
them to the output of \texttt{ltrace} proves the filters for
Listing~\ref{lst:hw} are correct.

\clearpage
\subsubsection{Advanced example}
\texttt{Auto-seccomp} does not detect syscalls or library calls via inline
assembly.
Therefore, remote code execution can be emulated by inline assembly calls, as
seen in Listing~\ref{lst:advanced-example}.
% Listing~\ref{lst:makefile}
% shows the Makefile used to compile the example with and without seccomp
% filters.
% Once \texttt{auto-seccomp} generates \texttt{auto-seccomp.h} and
% \texttt{auto-seccomp.c}, the generic Makefile compiles the source file.

\begin{listing}[!h]
  \inputminted[linenos,frame=lines]{c}{code/advanced-example/main.c.pre}
  \caption{A small program simulating remote code execution via inline
    assembly.}\label{lst:advanced-example}
\end{listing}

\begin{listing}[!h]
  \begin{minted}[linenos,frame=lines]{c}
    #include "auto-seccomp.h"
    #include <seccomp.h>

    void auto_seccomp_setup() {
      scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(fstat), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigreturn), 0);
      seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 0);
      seccomp_load(ctx);
      seccomp_release(ctx);
    }
  \end{minted}
  \caption{Automatically created seccomp filter rules for
    Listing~\ref{lst:advanced-example}.}
  \label{lst:ae-filters}
\end{listing}

\begin{samepage}
  Compiling Listing~\ref{lst:advanced-example} without seccomp filters prints
  a \texttt{fortune} quote after the initial \texttt{printf}:
\begin{verbatim}
    $ ./example
    Starting execution
    WYSIWYG:
            What You See Is What You Get.
\end{verbatim}
\end{samepage}

Listing~\ref{lst:ae-filters} shows the generated filters, using the same
generation process as mentioned above.

\begin{samepage}
  After another \texttt{make}, to compile the seccomp filters into the binary,
  executing it fails with \texttt{SIGSYS: Bad system call}:
\begin{verbatim}
  $ ./example
  Starting execution
  [1]    9599 invalid system call (core dumped)  ./example
\end{verbatim}
\end{samepage}

Since the inline assembly emulates an attacker, this example demonstrates that
\texttt{auto-seccomp} successfully mitigated malicious code execution.
However, if the attacker only uses white-listed syscalls, the program will not
be terminated. Therefore, it is essential to keep the seccomp white-list as
small as possible to minimize the risk of malicious actions.

\clearpage
% \subsection{Binutils}
% Compile different binutils with auto-seccomp tool

\subsection{Statistics}
The projects used for testing are:
\begin{enumerate}[font={\bfseries}]
\item kilo\footnote{\url{https://github.com/antirez/kilo}}:
  minimal text editor without external dependencies ($<$ 1.000 lines of
  code)
\item tte\footnote{\url{https://github.com/GrenderG/tte}}:
  minimal text editor without external dependencies
\item redis\footnote{\url{https://github.com/antirez/redis}}:
  in-memory data structures server
\item git\footnote{\url{https://github.com/git/git}}:
  distributed version-control system
  % \item postgres\footnote{\url{https://github.com/postgres/postgres}}
  % \item openssl\footnote{\url{https://github.com/openssl/openssl}}
\end{enumerate}

Table~\ref{tab:project-sizes} shows that the projects used for testing vary
significantly in size, in terms of files and lines of code.
These projects are only used to test the performance of \texttt{auto-seccomp},
not the correctness.
Given the current libc mapping, tte and kilo are fully usable, redis and git are
not.
All of the projects mentioned here use \texttt{make} as their build system.
\\

\begin{table}[h]
  \centering
  \begin{tabular}{|l||*{4}{r|}}
    \hline
    \thdr{Project}       & \tl{kilo} & \tl{tte} & \tl{redis} & \tl{git} \\
    \hline\hline
    \thdr{Files}         & 1         & 1        & 77         & 444      \\\hline
    \thdr{Lines of Code} & 958       & 1080     & 56490      & 193885   \\\hline
  \end{tabular}
  \caption{Project sizes}
  \label{tab:project-sizes}
\end{table}

\subsubsection{Compile time}\label{ssec:compile-time}
Adding \texttt{auto-seccomp} to an existing code base requires the following
steps:
\begin{enumerate}
\item generate \texttt{compile\_commands.json} (\ie \texttt{bear make})
\item run \texttt{auto-seccomp} on all source files listed in
  \texttt{compile\_commands.json}
\item Makefile:
  \begin{enumerate}
  \item add \texttt{auto-seccomp.o} to the list of targets (only for non-generic
    makefiles)
  \item add \texttt{-lseccomp} to linker flags
  \end{enumerate}
\end{enumerate}

All projects have been compiled ten times with seccomp filters, and
ten times without seccomp filters.
Also, \texttt{auto-seccomp} was used ten times in connection with each code
base.
Table~\ref{tab:compile-time} shows the average time for both operations.

For small code bases, such as tte and kilo, \texttt{auto-seccomp} is instant and
the increase in compile time is insignificant ($0.01 s$).
For a medium project like redis, \texttt{auto-seccomp} does add minimal
overhead ($3-4\ s$, roughly $9\%$ of total compile time).
However, the compilation overhead is still insignificant ($0.16 s$).
For complex and large projects, like git (more than 190.000 LoC),
\texttt{auto-seccomp} starts to use more time: $\sim 71 s$ amounts to approximately
$33 \%$ of total compile time. The compilation overhead is insignificant
($0.4s$), even for large code bases.


\begin{table}[h]
  \centering
  \begin{tabular}{|l||R{4em}|r||r|r|r|}
    \hline
    \multirow{2}{*}{\thdr{Project}}
    & \multicolumn{2}{c|}{\thdr{auto-seccomp time (s)}}
    & \multicolumn{2}{c|}{\thdr{Compile time (s)}}
    & \multirow{2}{*}{\thdr{Difference (s)}}\\ \cline{2-5}
    & $\mu$ & $\sigma$ & \textsf{original} & \textsf{with seccomp} & \\ \hline
    kilo  &  0.11 & 0.038 &   0.10 &   0.11 & 0.01 \\
    tte   &  0.11 & 0.035 &   0.11 &   0.12 & 0.01 \\
    redis &	 3.74 & 0.064 &  41.04 &  41.20 & 0.16 \\
    git   & 71.36 & 0.563 & 218.69 & 219.12 & 0.43 \\
    % postgres & & & & \\
    % openssl & & & & \\
    \hline
  \end{tabular}
  \caption{Compile times: average over ten runs (X1 Carbon 6th Gen. with Intel
    i7-8550U (8) @ 4GHz)}
  \label{tab:compile-time}
\end{table}

% \subsubsection{Syscall rate}

% concrete CVE that can be mitigated
% \subsection{Example}
