\section{Future Work}\label{future_work}

\subsection{Problems/Limitations}
\subsubsection{Libc mapping}\label{sssec:libc-mapping}
The most significant limitation concerning usability and correctness is the libc
mapping.
Without this mapping, \texttt{auto-seccomp} is unusable for most developers,
since hardly any application uses syscalls instead of libc functions (at least
for repetitive tasks, like printing and reading input).
Using the Clang tooling interfaces, it is challenging to generate an accurate
mapping without requiring any manual work.
If such a tool would exist, a glibc filter mapping could be easily generated.
Based on the implementation of specific libc functions, the possibility of
different syscalls depending on function arguments has to be considered as well.
To guarantee correctness, the function mapping needs to cover the entirety of
libc.

\subsubsection{Function pointers}
Since function pointers are treated as regular variables, name-checking can
produce a false negative.
Function pointers require specific treatment, as demonstrated in
Listing~\ref{lst:func_ptr}.
\begin{listing}[H]
  \begin{minted}[linenos,frame=lines]{c}
    char* src = "Hello World!";
    size_t len = strlen(src);

    void (*func_ptr)(const char*, size_t);
    func = &libc_function;

    (*func_ptr)(src, len);
  \end{minted}
  \caption[Function pointers (libc syscalls not recognized)]{Example code
    involving function pointers. \texttt{Auto-seccomp} will not recognize
    \texttt{func} as an alias for \texttt{libc\_function}.}\label{lst:func_ptr}
\end{listing}

The function pointer \texttt{func\_ptr} is pointing to an arbitrary libc function
(represented by \texttt{libc\_function}).
A name check for \texttt{func\_ptr} will not reveal that a call to
\texttt{func\_ptr} is a call to \texttt{libc\_function}, and therefore
needs the same syscalls added to the syscall list.
\texttt{Auto-seccomp} will not recognize such cases.
However, libTooling does provide everything that is needed to extend
\texttt{auto-seccomp} to resolve function pointers and perform a name check on
the original function.

\subsubsection{Unit testing}
Many projects employ a unit testing framework.
Sometimes test cases (ordinary C files) are located in the same directory
structure as the application source code.
This means that multiple definitions of \texttt{main} exist, since testing
requires standalone binaries.
To result in correct behaviour for \texttt{auto-seccomp}, one of the following
things must happen:
\begin{enumerate}
\item \texttt{compile\_commands.json} must not include compile information about
  the test cases
\item \texttt{auto-seccomp} is called on a subset of source files (only the
  original application source code)
\item \texttt{auto-seccomp} recognizes test files and skips them (either by
  automatically detecting testing frameworks or a special comment, \ie
  \mintinline{c}{// auto-seccomp: skip})
  \item pass the \texttt{main} file as additional argument (\ie
    \texttt{auto-seccomp --main-file=main.c file1.c file2.c ...})
\end{enumerate}
Currently, only 1. and 2. are possible options, 3. and 4. would require
additions to \texttt{auto-seccomp}.

\subsubsection{Testing}
This work is considered a proof of concept.
Given the sheer size of glibc, \texttt{auto-seccomp} needs extensive testing to
ensure complete coverage.
Testing for \texttt{auto-seccomp} has been selective and dependant on the
projects used with \texttt{auto-seccomp} (see Section~\ref{sec:results}).

\subsection{Improvements}
\subsubsection{Argument restriction}
Seccomp-bpf allows not only the restriction of system calls but also the
restriction of system call arguments.
\texttt{Auto-seccomp} could be extended in such a way, that it generates
additional argument restrictions.

For example: an application only uses \texttt{printf} and \texttt{scanf} to
interact with the user.
In this case, \texttt{auto-seccomp} would realize that \texttt{write} and
\texttt{read} are only used with \texttt{STDOUT} and \texttt{STDIN},
respectively, and generate the following argument filters:

\inputminted[linenos,frame=lines]{c}{code/argument-filters.c}

The automatic generation of argument filters is complicated, since certain calls
(\ie \texttt{dup2}) may alter the allowed arguments without changing the
input source or output target.

\subsubsection{Extensibility}


\begin{figure}
  \centering
  \input{tikz/flow-chart-extended.tex}
  \caption[Flowchart of the improved syscall detection logic.]{Flowchart of the
    improved syscall detection logic. Multiple libraries are supported.
  The  sequence \texttt{lib\_1} to \texttt{lib\_N} represents $N$ external
  libraries the application uses.}
  \label{fig:syscall-detection-extended}
\end{figure}

\paragraph{Additional libraries}
\label{sssec:extend-libs}
\texttt{Auto-seccomp} only supports small parts of glibc.
Full glibc support would mean \texttt{auto-seccomp} could be used in a wide
variety of projects without additional dependencies.
However, many C applications are dependent on a number of libraries.
For example, many applications providing a terminal user interface use a library
(like ncurses~\cite{ncurses}) to handle user interaction.
For \texttt{auto-seccomp} to be universally applicable, more libraries need to
be supported.

Figure~\ref{fig:syscall-detection-extended} illustrates how the extended library
support changes the detection mechanism of \texttt{auto-seccomp}.

\paragraph{Modular approach}
In order to support more libraries, a modular approach would be practical.
This means, instead of compiling a function mapping for one or multiple
libraries into the binary, files containing the function mapping (\ie in JSON
format) are passed to \texttt{auto-seccomp} via the command line.
Additionally, this would eliminate the need to recompile
\texttt{auto-seccomp} when changing the function mapping (\ie when debugging the
mapping) or adding a new function mapping for a new library.
Multiple files could be passed to \texttt{auto-seccomp} (\ie each
one representing one library).

\subsubsection{Setup function in \texttt{auto-seccomp.h}}
As mentioned in Section~\ref{ssec:compile-time}, the developer manually adds the
object file \texttt{auto-seccomp.o} to the build system.
To reduce the degree of manual intervention even further, \texttt{auto-seccomp}
could generate a single header file which contains both the function declaration
and implementation.
Once included in the \texttt{main} file, this function would be compiled
automatically as part of the original build system.