\section{Design}\label{sec:design}
In this section, we discuss the challenges associated with developing
\texttt{auto-seccomp}.
We describe the problems arising in connection with syscall detection and
seccomp filter generation.
Finally, we show how these problems can be solved and outline the features of
\texttt{auto-seccomp}.

\subsection{Challenges}
The tool can be broken down into three parts:
\begin{enumerate}
\item Parse the source code
\item Analyze source code and find all used syscalls
\item Given the list of syscalls, generate the corresponding seccomp filters
\item Refactor the source code to include the filters
\end{enumerate}

Section~\ref{ssec:parsing} talks about choosing the right interface to parse the
source code.
Section~\ref{ssec:analysis} describes how \texttt{auto-seccomp} uses static
code analysis to extract a list of used syscalls.
Section~\ref{ssec:seccomp-filters} illustrates the transformation of the
syscall list to the final seccomp filters.
Section~\ref{ssec:refactoring-design} shows how the source code changes
during the run of \texttt{auto-secccomp}, effectively keeping manual
intervention low.
Finally, Section~\ref{ssec:overview} gives an overview of the different parts of
\texttt{auto-seccomp}.

\subsection{Parsing the source code}\label{ssec:parsing}
In Section~\ref{sssec:tooling}, we described the differences between plugins,
libTooling and libClang.
All of these frameworks parse the source code like the Clang compiler would,
rendering them possible options for \texttt{auto-seccomp}.
\texttt{Auto-seccomp} uses static source code analysis. This means the internal
representation of the source code (\ie the AST) is the basis for the analysis
and relevant data extracted from this representation.
It is possible to write a parser from scratch, but C and C++ code is not trivial
to parse.
See Section~\ref{ssec:choosing-interface} for more information on the
implementation.

\subsection{Source code analysis}\label{ssec:analysis}
After parsing the source code, all further actions are working on the AST.
Essentially, two different detection mechanisms need to be deployed:
\emph{direct} syscall detection (direct syscalls meaning a call to a wrapper
function, like \texttt{open} for the syscall \texttt{open}) and \emph{indirect}
syscall detection (or libc syscall detection).

\subsubsection{Direct syscall detection}
Direct syscall detection consists of parsing the source code and name checking
wrapper functions (\eg the wrapper function \texttt{open} for the syscall
\emph{open}).

\paragraph{Syscall name list}
The Linux kernel repository contains a list of syscall definitions for the x86
architecture (64-bit) called
\texttt{syscall\_64.tbl}\footnote{\url{https://github.com/torvalds/linux/blob/master/arch/x86/entry/syscalls/syscall_64.tbl}
  (Accessed: \DTMdisplaydate{2018}{11}{27}{-1})}.
All the syscall names for \texttt{auto-seccomp} were taken from this list
and compiled into the binary.
\texttt{Auto-seccomp} checks the name of function calls against this list.

\begin{figure}
  \centering
  % \missingfigure{Flowchart of syscall detection (like the first draft of presentation).}
  \input{tikz/flow-chart.tex}
  \caption{Flowchart of the syscall detection logic.}
  \label{fig:syscall-detection}
\end{figure}

\subsubsection{Indirect syscall detection}\label{sssec:libc-detection}
Syscall detection via libc functions proves to be more complicated than direct
syscall detection.
Instead of just matching a function name to a list of known syscalls,
\texttt{auto-seccomp} needs a mapping of libc function names to used syscalls.

The tool needs an extensive list of all libc functions to guarantee correctness.
Because the Clang compiler and the GNU libc (glibc) are not compatible, the
creation of this list is tedious.
This work could easily be automated with a Clang tool if it were possible to
compile glibc with Clang.

When testing with \texttt{ltrace}, such a list can be composed.
A test program uses functionalities provided by libc (like locking) and
\texttt{ltrace} prints out the syscalls used upon entering the libc code base.

% Using \texttt{ltrace} and test programs, such a list can be composed. The test
% program uses functionality like locking and \texttt{ltrace} prints out the
% syscalls used upon entering libc code.

Once the analysis by \texttt{auto-seccomp} reveals a libc function call, the
tool resolves the function call into syscalls by performing a look-up in the
function mapping.
Currently, this mapping is compiled into \texttt{auto-seccomp}, see
Section~\ref{sssec:extend-libs} for improvement suggestions.

Figure~\ref{fig:syscall-detection} demonstrates how \texttt{auto-seccomp}
determines whether a call is a syscall or contains syscalls inside the libc
code.
This procedure is repeated for every call in the source code.

\subsection{Seccomp filters}\label{ssec:seccomp-filters}
\texttt{Auto-seccomp} starts with a minimal white-list containing the
syscalls \texttt{read}, \texttt{write}, \texttt{rt\_sigreturn} and
\texttt{exit/exit-group}.
The selection of these syscalls is based on the first version of seccomp,
explained in Section~\ref{ssec:bg-seccomp}.
Generally, it is the bare minimum of needed syscalls for an application.

For each additional syscall in the list, \texttt{auto-seccomp} generates the
corresponding \linebreak seccomp-bpf code to add it to the filter list.
Additionally, \texttt{auto-seccomp} dumps the list to a new file.
This new file is the basis for the automatic inclusion in the build process.

\subsection{Refactoring}\label{ssec:refactoring-design}
\texttt{Auto-seccomp} refactors the application source code after the syscall
analysis ends.
To keep the degree of manual intervention as low as possible,
\texttt{auto-seccomp} injects code snippets which automatically handle
the setup of seccomp mode.
% everything related to seccomp.
After recompiling the project, the application is secured with seccomp.
Furthermore, \texttt{auto-seccomp} checks whether the call to the setup function
or the include directive is already present.
The code snippets are not added if these mechanisms detect their presence in the
code (otherwise resulting in an erroneous program).
\texttt{Auto-seccomp} corrects the code if one of the snippets is missing.

\subsection{\texttt{auto-seccomp} overview}\label{ssec:overview}
As mentioned in Figure~\ref{fig:uml-seccomp-tool}, \texttt{auto-seccomp} was
extended to include a \emph{SyscallManager}.
The \emph{SyscallManager} handles the following:
\begin{itemize}
\item detection of direct syscalls
\item detection of syscalls used by libc
\item management of an up-to-date syscall list
\item generation of the final seccomp filters (including file and function
  names)
\end{itemize}
