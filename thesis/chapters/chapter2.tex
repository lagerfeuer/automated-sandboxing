\section{Background}\label{background}

\subsection{Sandboxing}
Sandboxing is a security technique used to shield and protect users from
malware. The most prominent applications of sandboxing are browsers and
smartphones.
In browsers, opening a website executes JavaScript code in the vast majority
of cases~\cite{web-statistics}.
% (about 95\% of websites use Javascript)
Without restrictions, a malicious website could access or manipulate system
resources. This is why browsers execute untrusted code inside a \emph{sandbox}
\cite{edge_googles_2009, greamo_sandboxing_2011}.

Mobile operating systems make use of sandboxing as well, although the mechanisms
differ.
By default, Android applications run in a dedicated process and have no access
to the file system besides a single directory, created for that specific
application \cite{elenkov_android_2014}.
Additionally, Android has a permission system in place.
The default actions are minimal. Therefore every application declares
needed permissions and users can check these permissions before installing.
In recent Android versions, the user can grant or deny permissions to
different applications on a per-app basis at runtime.
Older versions require the user to accept all permissions at
install-time~\cite{google_android-permissions}.

\subsection{Linux System Calls}
% http://ece.ubc.ca/~brucew/ebook/AdvancedLinuxProgramming/
The kernel is the most crucial part of every operating system -- controlling
the system's resources, hardware and peripherals. Therefore, the kernel must
always be protected and cannot be permitted to execute unsafe user code.
However, the user applications must be allowed to request resources and services
from the operating system.

Modern operating systems solve this problem by exposing a set of
\emph{System Calls (Syscalls)} to the user.
Syscalls are pre-defined routines that are safe to run in privileged mode
(\emph{kernel mode}) and make up the interface used by programs to request
resources or services from the kernel~\cites{tanenbaum_modern_2009,
  mitchell_advanced_2001, silberschatz_operating_2014}.

Whenever the user application wants to access system resources, the process must
first issue a syscall (\eg to read a file, the application has to call either
\texttt{open} or \texttt{openat}).
Some libraries offer higher level abstractions of underlying structures and
operations (easier handling of resources) or additional functionality.
These libraries use syscalls underneath (\eg \texttt{fopen} in libc uses
\texttt{open}/\texttt{openat}).
Tools such as \texttt{ltrace} and \texttt{strace} can provide information about
the used syscalls of library functions, or binaries in general, without
access to the source code.

\subsection{Seccomp}\label{ssec:bg-seccomp}
Seccomp (\emph{Secure Computing}) is a security utility built into the Linux
kernel that allows a simple version of sandboxing for user applications.
In the first version of seccomp, all syscalls were blocked except \texttt{read},
\texttt{write}, \texttt{sigreturn} and \texttt{exit}.
Since then, seccomp received an upgrade called \emph{seccomp-bpf}.
Seccomp-bpf allows developers to create a syscall filter list within the source code,
selectively white-listing syscalls.
All other syscalls are black-listed by default and calling them leads to the
termination of the application.
Seccomp-bpf is the base for many sandboxing and virtualization techniques
\cite{kim_practical_2013, docker_seccomp_2018}.

Listing~\ref{lst:seccomp-example} shows an example of a standard seccomp filter
list.
Essentially, an application that interacts with the user needs at least
\texttt{exit/exit\_group}, \texttt{read}, \texttt{write} and
\texttt{rt\_sigreturn}.
Importantly, library functions providing higher-level interfaces than plain
syscalls for actions like printing text (\eg \texttt{printf}) can use additional
syscalls in the background.
White-listing \texttt{write} might not be enough, leading the application to
terminate.

By using seccomp, we can also enforce restrictions on the arguments passed to
the syscalls.
For example, if the application is not allowed to write to anything but
\texttt{STDOUT}, then \texttt{STDOUT} can be specified as the only valid
argument (Listing~\ref{lst:seccomp-example}, line 12).
This can be specified via the \texttt{SCMP\_A\{0-5\}} macros, to select which
argument to restrict, and the \texttt{SCMP\_CMP\_XX} macros, to select a
mathematical operation for the comparison (by replacing \texttt{XX} with
the operation of choice, \eg \texttt{EQ} for \emph{equal}).
All syscalls not included in the list are blocked after the list is
loaded into the kernel (Listing~\ref{lst:seccomp-example}, line 13).

\begin{listing}
  \centering
  \inputminted[linenos,frame=lines]{c}{code/seccomp-example.c}
  \caption{Seccomp example}
  \label{lst:seccomp-example}
\end{listing}


\subsection{Clang and LLVM}
This section introduces the Clang and LLVM compiler framework.
The LLVM infrastructure combines three stages (\emph{front end},
\emph{optimizer} and \emph{back end}) into a fully functional compiler.
LLVM enables developers to easily integrate custom parts for new
languages or new hardware into an otherwise established compiler chain.
Clang offers a variety of information about C and C++ source code which can be
accessed with the help of the Clang plugin framework or one of the standalone
tooling frameworks (libClang and libTooling).

\subsubsection{LLVM}
Low Level Virtual Machine (LLVM) % \footnote{\url{http://www.llvm.org}}
is a compiler framework and infrastructure.
Starting as a compiler and toolchain research project, LLVM is now widely used
by many companies and projects.
Due to its language-independent design, LLVM is easily extensible to support new
languages and architectures.
LLVM achieves this by splitting the compiler into three parts: the \emph{front
  end}, the LLVM \emph{intermediate representation} (IR) and the \emph{back end}
\cite{lattner_llvm:_2002, lattner_llvm:_2004, lattner_llvm_2008,
  noauthor_llvm_nodate}.

A front end transforms the source code to LLVM IR.
The LLVM core then runs multiple passes on the IR, optimizing the code.
Once the LLVM core outputs the optimized IR, the back end takes over.
The back end uses the IR to generate a target-specific binary, further
optimizing the code by mapping the IR instructions as efficiently as possible to
the specified architecture (see Figure~\ref{fig:llvm-overview}).

Many companies and projects have used the LLVM infrastructure to create new
languages on top of the LLVM core or to support new architectures, like
microcontrollers or consoles, by writing \emph{front} or \emph{back ends}.
Popular custom front ends are Haskell \cite{terei_llvm_2010}, Rust, and Swift.

\begin{figure}
  \centering
  \input{tikz/llvm.tex}
  \caption{Compilation using the LLVM compiler framework.}
  \label{fig:llvm-overview}
\end{figure}

Additionally, LLVM brings its own stack of tools to be used as a drop-in
replacement for the GNU Compiler Collection (GCC):\\\\
\begin{minipage}{.5\linewidth}
  \begin{itemize}
  \item Compiler: \texttt{clang}
  \item Debugger: \texttt{lldb}
  \item Linker: \texttt{lld}
  \end{itemize}
\end{minipage}
\hfill
\begin{minipage}{.5\linewidth}
  \begin{itemize}
  \item C++ STL: \texttt{libc++}
  \item various sanitizers
  \end{itemize}
\end{minipage}\\[2em]
In the beginning, LLVM was developed for C and C++. Since then, many front
ends have been written and the C and C++ specific parts moved to the
Clang project.

\subsubsection{Clang}
Clang % \footnote{\url{https://clang.llvm.org}}
is an LLVM front end for C-family languages (C/C++/Objective-C)
\cite{noauthor_clang_nodate-1, lattner_llvm_2008}.
The name Clang is also used to describe a collection of source code analysis and
refactoring tools, \eg \texttt{clang-format} or \texttt{clang-check}.
These tools were written using the Clang tooling framework, which aims to
expose compiler internals to provide the possibility for in-depth analysis and
refactoring \cite{duffy_exploiting_2014, junod_obfuscator-llvmsoftware_2015}.
The Clang project itself provides a number of tools to increase code quality and
make debugging more comfortable, such as
\texttt{clang-check}~\cite{noauthor_clang::clang-check_nodate},
\texttt{clang-format}~\cite{noauthor_clang::clang-format_nodate}
and \texttt{clang-tidy}~\cite{noauthor_clang::clang-tidy_nodate}.
% Using the Clang tooling framework and knowledge about the Abstract Syntax Tree,
% standalone tools or compiler plugins (exclusively for use with the Clang
% compiler) can be created.

\begin{listing}[!b]
  \footnotesize
  \centering
  \begin{alltt}
    \input{misc/ast.tex}
  \end{alltt}
  \caption{AST printed by \texttt{clang -Xclang -ast-dump -fsyntax-only ast.c}}\label{lst:ast-print}
\end{listing}

\clearpage

% \begin{minipage}[t]{.45\linewidth}
\begin{multicols}{2}

  \subsubsection{Abstract Syntax Tree}\label{sssec:ast}
  In order to verify the correctness of a program, the compiler generates what is
  called an \emph{Abstract Syntax Tree} (AST).
  The AST is the compiler's internal representation of the program and includes all
  parts of the program (\eg identifiers, function declarations, function calls,
  ...) as nodes in a tree.
  The connections between nodes symbolize dependencies. Clang has a very
  rich AST, closely resembling the original source code (\ie little or no
  abstractions) \cite{noauthor_introduction_nodate, klimek_clang_2013}.

  \begin{listing}[H]
    \centering
    \inputminted[linenos,frame=lines]{c}{code/ast.c}
    \caption{AST example (\texttt{ast.c}).}
    \label{lst:ast}
  \end{listing}


  Listing~\ref{lst:ast} shows a small example program. Printing the AST is
  done by either invoking \texttt{clang-check} with the \texttt{-ast-dump}
  option, or by calling \texttt{clang} during compilation with \texttt{-Xclang
    -ast-dump}. Listing~\ref{lst:ast-print} shows the output of \texttt{clang},
  the AST created by the compiler. Figure~\ref{fig:ast-tree} presents the AST
  in a more readable manner. A TranslationUnitDecl is Clang's top-level
  declaration of a single translation unit\footnote{A translation unit is a
    single file, after all preprocessor related tasks have been completed (\ie
    all defines, includes, ifdefs and macros have been resolved).
    Each translation unit compiles into an object file.}.
  Top level statements and expressions are directly accessible from the
  TranslationUnitDecl.

  \columnbreak
  % \end{minipage}
  % \hfill
  % \begin{minipage}[t]{.5\linewidth}
  \begin{figure}[H]
    \centering
    \input{tikz/ast-tree.tex}
    \caption{Diagram of the parsed AST for \texttt{ast.c}.}
    \label{fig:ast-tree}
  \end{figure}
\end{multicols}
% \end{minipage}

% \begin{figure}[H]
%   \centering
%   \input{tikz/ast.tex}
%   \caption{Diagram of AST}
%   \label{fig:ast-tikz}
% \end{figure}

\paragraph{Node Types} As mentioned in Section~\ref{sssec:ast}, Clang provides a
detailed representation of the code.
The AST closely matches the actual source code.
To ensure this level of detail while keeping the AST and its types structured,
Clang makes excessive use of inheritance and polymorphism.
Everything is derived from four basic classes: \emph{Decl} (Declaration),
\emph{Stmt} (Statement), \emph{Type} and \emph{Expr} (Expression)
\cite{klimek_clang_2013, goldsborough_clang-useful:_2017}.

% Figure~\ref{fig:functiondecl-inheritance} shows how \emph{FunctionDecl}
% inherits from \emph{Decl}.
\paragraph{Example} \emph{FunctionDecl} inherits from \emph{Decl} as follows
(Figure~\ref{fig:functiondecl-inheritance}):
\begin{enumerate}
\item \embf{Decl} is a general declaration of any kind
\item \embf{NamedDecl} describes a declaration that may have a name (Note: may,
  not must)
\item \embf{ValueDecl} is reserved for a subclass of declarations:
  variable declaration, function declaration or enum constant
\item \embf{FunctionDecl} represents the actual function declaration
\end{enumerate}

Given the size and complexity of the AST, Clang deploys various techniques to
keep the memory usage as low as possible.
Classes use pointers to other class instances to provide access to information
stored indirectly (\eg \emph{CallExpr} has a pointer to the matching
\emph{FunctionDecl}).
In most cases, \emph{CallExpr} has a \emph{FunctionDecl}.
The information exists only once, \emph{CallExpr} directly references the
corresponding \emph{FunctionDecl} (or a \texttt{nullptr} in some instances).

Furthermore, each node type has a variety of methods, which can be used to query
attributes, such as source locations (important for refactoring the source code)
or function names (\eg to check whether a call is a syscall).
In order to check the name of a \emph{CallExpr}, we have to get its
corresponding \emph{FunctionDecl}, then we can query the \emph{FunctionDecl} for
the name (\texttt{getNameInfo()}).

\begin{figure}
  \centering
  \input{tikz/functiondecl-inheritance.tex}
  \caption{Inheritance chain of \texttt{FunctionDecl}.}\label{fig:functiondecl-inheritance}
\end{figure}

\subsubsection{Clang Interfaces}\label{sssec:tooling}
Clang exposes all the information it has on the AST and can be used as a library
to build plugins or tools. We can choose from three different
options~\cite{noauthor_choosing_nodate}:
% Each one of the options has its advantages and disadvantages.

\newif\ifcols
% \colstrue

\ifcols\begin{multicols}{3}\fi
  \paragraph{Plugins}
  Plugins are written in C++, compiled to shared libraries and loaded whenever
  the compiler is invoked (specified via \texttt{-Xclang} command line options).
  Typically, plugins are used to generate custom warnings and
  errors~\cite{noauthor_clang_nodate}.
  For example, enforcing a company internal coding standard.
  The plugin can analyze the code, use Clang diagnostics to print a warning or
  error message and even break the build if necessary.
  Since the plugin gets invoked with each call to the compiler, there is no easy
  way to store information across the entire source tree, or even realize which
  file is first or last in the build process.
  \ifcols\columnbreak\fi
  \paragraph{LibTooling}
  LibTooling is a C++ interface as well \cite{noauthor_libtooling_nodate}.
  However, libTooling produces standalone tools, that do not need to invoke the
  compiler.
  Therefore, tools do not trigger a new build.
  All source files (or a subset of source files) are passed to the tool.
  Before the tool runs the custom analysis or refactoring code, the AST is
  created. This means the tool must first know about all compiler flags and
  definitions.
  There are two ways to achieve this: either passing all flags to the tool
  after the \verb+--+ separator, or generating a \texttt{compile\_commands.json}
  file \cite{noauthor_json_nodate}.
  Calling the tool on a set of source files makes it easy to keep information
  across files (\eg for global statistics or determining the first or last
  file).
  \ifcols\columnbreak\fi
  \paragraph{LibClang}
  LibClang provides both a C and Python interface~\cite{noauthor_libclang:_nodate}.
  Compared to libtooling, libClang offers a higher level view of the AST.
  This means that not everything is accessible, but less code is needed for
  tasks that can be achieved with libClang.
  In terms of usability, libClang can replace libTooling, if the reduced
  interface satisfies the application requirements.
  \ifcols\end{multicols}\fi