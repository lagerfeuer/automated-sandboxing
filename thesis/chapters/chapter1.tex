\section{Introduction}\label{introduction}
Sandboxing is a security method used to increase protection against threats,
such as software bugs or malicious code.
This is achieved by running applications in a separate or locked-down
environment; thereby restricting the application to specific actions or
simulating a different machine entirely.
Since the application is running in a restricted mode, an attacker has
limited resources to work with if the application becomes compromised.
After everything else fails, sandboxing can be the last line of defense
against an attacker.

Different approaches to sandboxing with varying degrees of isolation exist. In
this thesis, we talk exclusively about Seccomp (\emph{Secure Computing}),
the Linux kernel's implementation of a basic sandboxing mechanism
\cite{corbet_seccomp_2009}.

Seccomp filter rules can put an application in a \emph{sandbox mode},
isolating the process and restricting allowed actions (\ie system calls) based
on a simple set of rules within the source code.

Limiting an application to the bare minimum of system calls (\emph{syscalls})
for a particular use case means an attacker cannot exploit the application to
make arbitrary syscalls.

A wide variety of devices and software packages running on Linux utilize these
filters to reduce the risk of software vulnerabilities or untrusted code.
Well-known examples are the Android operating system,
various browsers (Chrome~\cite{edge_googles_2009},
Firefox~\cite{mozilla_sandbox_nodate}),
virtualization software (Docker~\cite{docker_seccomp_2018})
and package management systems (Snap~\cite{snap_security_nodate},
Flatpak~\cite{flatpak_security_nodate}).
With the rapidly increasing usage of the web, browser developers have put a lot
of work into efficient sandboxing to shield the user from malicious web pages.

Developers have to create seccomp filter lists manually, which is tedious
and repetitive. Human errors are possible and may lead to bad user experience.
The filters might be too restrictive or give an attacker more resources
to work with than the application needs to function, meaning the filters are too
permissive.
Static code analysis can solve these problems by automating the creation of the
filter list and guaranteeing an accurate and minimal list of allowed syscalls.

In this bachelor thesis, we present a tool based on the Clang/LLVM compiler
framework, called \texttt{auto-seccomp}, to automatically generate seccomp
filters.
With little effort, \texttt{auto-seccomp} can be included in an existing build
system to generate seccomp filters on demand or every time the project is
compiled.

\texttt{Auto-seccomp} performs source code analysis to detect all used syscalls.
Syscalls used by a subset of libc functions are detected as well.
Furthermore, we illustrate that \texttt{auto-seccomp} adds little overhead to
the compilation process and is easily extensible to include more libraries or
additional functionality.

% Given the rich interface the Clang compiler framework provides us, the tool can
% be extended in a number of ways.
% We discuss this in Section \ref{future_work}.


% NOT IN THE PAPER
\iffalse
\subsection{Structure of this document}\label{structure}
Section \ref{background} provides all the necessary knowledge for this thesis.
Section \ref{design} explains how the tool was planned to be used,
while section \ref{implementation} explains the implementation details and
provides information related to the tool.
Section \ref{evaluation} shows some examples that have been compiled using
automatically generated seccomp filters and measures multiple characteristics,
like the overhead in compilation.
Section \ref{future_work} has some pointers for future work and describes areas
of improvement and lists current limitations and problems.
Section \ref{conclusion} provides a conclusion and summarizes this work.
\fi