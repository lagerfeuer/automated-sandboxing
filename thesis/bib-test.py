#!/bin/python

import os
import re
from colorama import init, Fore, Back, Style
from TexSoup import TexSoup

init(autoreset=True)


def red(t): return Fore.RED + str(t) + Fore.RESET


def green(t): return Fore.GREEN + str(t) + Fore.RESET


BIBLIOGRAPHY = 'bibliography.bib'
TEX_DIR = 'chapters/'


with open(BIBLIOGRAPHY, 'r') as f:
    bib_text = [e for e in f.readlines() if len(
        e) and e[0] == '@' and not e.startswith("@Comment")]
for i, e in enumerate(bib_text):
    bib_text[i] = e[e.find('{') + 1:-2]

# print(bib_text, end='\n\n')

tex_text = []
for c in [TEX_DIR + f for f in os.listdir(TEX_DIR) if f.endswith('.tex')]:
    with open(c, 'r') as f:
        soup = TexSoup(f.read())
        cites = list(soup.find_all('cite')) + list(soup.find_all('cites'))
        if not cites:
            continue
        refs = []
        for cite in cites:
            # print('###' + repr(cite) + '###')
            refs += re.findall('({.*?})+', repr(cite), re.DOTALL)
        for i, e in enumerate(refs):
            tex_text += e[1:-1].replace(',', '').split()
# print(tex_text, end='\n\n')

bib_set = set(bib_text)
tex_set = set(tex_text)
diff_set = bib_set - tex_set

if(len(diff_set)):
    print(Style.BRIGHT + "# %s missing out of %s" %
          (red(len(diff_set)), green(len(bib_set))))
    for e in sorted(diff_set):
        print("* %s" % e)
else:
    print(Style.BRIGHT + Fore.GREEN + "[+] 0 references missing!")
