#include <stdio.h>
#include <stdlib.h>

void execute() {
  char *args[2];
  args[0] = "fortune";
  args[1] = NULL;
  asm("call execvp" ::"D"(args[0]), "S"(args));
}

int main() {
  printf("Starting execution\n");
  execute();
  return 0;
}
