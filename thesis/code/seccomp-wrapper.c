#include "auto-seccomp.h"
#include <seccomp.h>

void auto_seccomp_setup() {
  scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, <syscall_name_1>, 0);
  // ...
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, <syscall_name_N>, 0);
  seccomp_load(ctx);
}
