class SeccompVisitor : public clang::RecursiveASTVisitor<SeccompVisitor> {
public:
  explicit SeccompVisitor(clang::ASTContext* Context, clang::Rewriter& Rewriter)
      : Context(Context), Rewriter(Rewriter) {}
  bool VisitFunctionDecl(clang::FunctionDecl* FD) { return true; }
private:
  clang::ASTContext* Context;
  clang::Rewriter& Rewriter;
};

class SeccompConsumer : public clang::ASTConsumer {
public:
  explicit SeccompConsumer(clang::ASTContext* Context, clang::Rewriter& Rewriter)
      : Visitor(Context, Rewriter) {}
  virtual void HandleTranslationUnit(clang::ASTContext& Context) {
    Visitor.TraverseDecl(Context.getTranslationUnitDecl());
  }
private:
  SeccompVisitor Visitor;
};

class SeccompAction : public clang::ASTFrontendAction {
public:
  std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance& CI, llvm::StringRef FileName) override {
    return std::unique_ptr<clang::ASTConsumer>(
        new SeccompConsumer(&CI.getASTContext(), Rewriter));
  }
private:
  clang::Rewriter Rewriter;
};

class SeccompToolFactory : public clang::tooling::FrontendActionFactory {
  clang::FrontendAction* create() override { return new SeccompAction; }
};

int main(int argc, const char** argv) {
  using namespace clang::tooling;
  CommonOptionsParser OptionsParser(argc, argv, SeccompCategory);
  ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
  return Tool.run(new SeccompToolFactory());
}
