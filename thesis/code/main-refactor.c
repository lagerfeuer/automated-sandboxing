#include <stdio.h>
#include <stdlib.h>
#include "auto-seccomp.h"

int main(int argc, char* argv[]) {
  // AUTO GENERATED
  auto_seccomp_setup();
  // rest of original application code
  // ...
  return 0;
}
