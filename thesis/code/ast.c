#define D 5
const char mask = 0xF0;

char func(int in) {
  return (in + D) & mask;
}
