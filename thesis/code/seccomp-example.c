#include <unistd.h>
#include <string.h>
#include <seccomp.h>

int main(int argc, char* argv[]) {
  scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigreturn), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
  seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write),
      1, SCMP_A0(SCMP_CMP_EQ, STDOUT_FILENO));
  seccomp_load(ctx);
  char* test_out = "Test Out\n";
  write(STDOUT_FILENO, test_out, strlen(test_out));
  return 0;
}
