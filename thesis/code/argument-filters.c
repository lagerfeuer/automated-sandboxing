seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write),
    SCMP_A0(SCMP_CMP_EQ, 1));
seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read),
    SCMP_A0(SCMP_CMP_EQ, 0));
