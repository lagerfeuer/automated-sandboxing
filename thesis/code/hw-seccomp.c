#include <stdio.h>
#include "auto-seccomp.h"

int main() {
  // AUTO GENERATED
  auto_seccomp_setup();

  printf("Hello World!\n");
  return 0;
}
