\documentclass[11pt, a4paper]{article}
\usepackage[a4paper]{geometry}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{parskip}
\usepackage{paralist} % compactenum
\usepackage{multicol}
\usepackage{alltt}
\usepackage[dvipsnames]{xcolor}
\usepackage[inline]{enumitem}
\usepackage[toc,page]{appendix}

\usepackage{nomencl}
\makenomenclature
\renewcommand{\nomname}{List of Symbols}

% math
\usepackage{amsmath, amsfonts, amsthm, amssymb}
\begingroup % to fix spacing before theorems
\makeatletter
\@for\theoremstyle:=definition,remark,plain\do{%
  \expandafter\g@addto@macro\csname th@\theoremstyle\endcsname{%
    \addtolength\thm@preskip\parskip
  }%
}
\endgroup

\usepackage{perpage}
\MakePerPage{footnote}

% \usepackage{mathabx} % bigboxplus, but clashes \ggg
\newcommand{\bigboxplus}{\boxplus}
\newcommand{\embf}[1]{\textbf{\emph{#1}}}

% graphics
% \usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,shapes.multipart,shapes.misc,arrows,trees,positioning,backgrounds,fit}
\usepackage{color}
\usepackage{subcaption}
\usepackage[labelfont=bf,margin=1cm]{caption}
\usepackage[section]{placeins} % float barriers
\usepackage{comment}
% referencing
\usepackage{hyperref}
% Bibliography
\usepackage[useregional]{datetime2}
\usepackage[backend=bibtex,urldate=long]{biblatex}
\usepackage{url}
% \usepackage{cite}
% \bibliographystyle{unsrt}
\bibliography{bibliography}

\usepackage{todonotes}

% tables
% \usepackage{tabularx}
\usepackage{multirow}
\usepackage{array}
\newcommand{\thdr}[1]{\textbf{\sffamily #1}}
\newcommand{\tl}[1]{\multicolumn{1}{l|}{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

% front page
\usepackage{eso-pic}

% Headers
\usepackage{titlesec}

\titleformat*{\section}{\LARGE\bfseries\sffamily}
\titleformat*{\subsection}{\Large\bfseries\sffamily}
\titleformat*{\subsubsection}{\large\bfseries\sffamily}
\titleformat*{\paragraph}{\large\bfseries\sffamily}
\titleformat*{\subparagraph}{\large\bfseries\sffamily}

% Code
\usepackage[outputdir=latexmk.out]{minted}
% \usemintedstyle{colorful}

% AST
\newcommand{\decl}[1]{\textbf{\color{Green}#1}}
\newcommand{\expr}[1]{\textbf{\color{Purple}#1}}
\newcommand{\hl}[1]{\textcolor{Orange}{#1}}
\newcommand{\func}[1]{\textbf{\color{Cerulean}#1}}
\newcommand{\type}[1]{\textcolor{ForestGreen}{#1}}
\newcommand{\cast}[1]{\textcolor{RubineRed}{#1}}
\newcommand{\lval}[1]{\textcolor{Blue}{#1}}

%%%%% TIKZ %%%%%%%%%%%%%%%%
% LLVM
\tikzstyle{block} = [rectangle, draw, line width=.5mm,
text width=7em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex', very thick, ->, line width=1mm]
% AST
% \tikzstyle{node} = [rectangle, draw, line width=.5mm,
% text width=6em, text centered, rounded corners, minimum height=3em]
\tikzstyle{node} = [circle, draw, line width=.5mm,
text width=4.5em, text centered]
\tikzstyle{path} = [draw, -latex', ->, line width=.6mm]

% flow chart
\tikzstyle{basic} = [rectangle, draw, line width=.3mm,
text width=7em, text centered, rounded corners, minimum height=4em]
\tikzstyle{decision} = [diamond, draw, line width=.3mm, text width=5em, text
centered]
\tikzstyle{special} = [circle, draw, line width=.3mm, text width=3em, text
centered]
\tikzstyle{flow} = [draw, -latex', thick, ->, >=stealth']
\tikzstyle{yes} = [draw, -latex', thick, ->, >=stealth', color=ForestGreen]
\tikzstyle{no} = [draw, -latex', thick, ->, >=stealth', color=red]

%%%%% VARIOUS SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\myauthor}{Lukas Deutz}
\newcommand{\mytitle}{Automated Sandboxing}
\newcommand{\myworktitle}{Bachelor Thesis}  %% official type of work like ``Master theses''
\newcommand{\mygrade}{Bachelor of Science} %% title you are getting with this work like ``Master of ...''
\newcommand{\mystudy}{Computer Science} %% your study like ``Arts''
\newcommand{\myuniversity}{Graz University of Technology} %% your university/school
\newcommand{\myinstitute}{Institute for Applied Information Processing and Communications} %% affiliation
\newcommand{\mysupervisor}{Michael Schwarz} %% your supervisor
\newcommand{\myhometown}{Graz} %% your home town
\newcommand{\mysubmissionmonth}{March} %% month you are handing in
\newcommand{\mysubmissionyear}{2019} %% year you are handing in
\newcommand{\mysubmissiontown}{\myhometown} %% town of handing in (or \myhometown)

\newcommand{\ie}{\textit{i.\,e.,\ }}
\newcommand{\eg}{\textit{e.\,g.,\ }}

\title{\mytitle}
\author{\myauthor}
\date{\today}

\hypersetup{plainpages=false,colorlinks,citecolor=black,filecolor=black,linkcolor=black,urlcolor=black}

% \usepackage{url}
\usepackage{listings}
\usepackage[xindy,nonumberlist,toc]{glossaries}
\colorlet{hlcolor}{Goldenrod!20!white}
\makeglossaries
%%%%% DOCUMENT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
% \addtocontents{toc}{\vspace{-1cm}}
\renewcommand*{\thefootnote}{\fnsymbol{footnote}}
\input{header}
\renewcommand*{\thefootnote}{\arabic{footnote}}

% Introduction
\input{chapters/chapter1}
\clearpage
% Background
\input{chapters/chapter2}
\clearpage
% Design
\input{chapters/chapter3}
\clearpage
% Implementation
\input{chapters/chapter4}
\clearpage
% Evaluation
\input{chapters/chapter5}
\clearpage
% Future Work
\input{chapters/chapter6}
\clearpage
% Conclusion
\input{chapters/chapter7}
\clearpage

\appendix
\input{chapters/chapter8}
\clearpage

\newpage
\cleardoublepage
\newpage
\phantomsection % if hyperref is used

% word list
% \addcontentsline{toc}{section}{Glossary}
% \printglossaries

% \bibliographystyle{unsrt}
% \bibliography{bibliography}

% \nocite{*}
\printbibliography

\end{document}
