#!/bin/bash

MUSL="musl"
if [ ! -d "$MUSL" ]; then
  git clone git://git.musl-libc.org/musl
fi

pushd "$MUSL"

BIN="libc-seccomp-filters"
LIBC_FILES=$(grep "\"file\":" compile_commands.json | cut -d':' -f2 | sed 's/^ //;s/"//g')

cp ../build/$BIN $BIN
./$BIN $LIBC_FILES > musl-filters.txt
popd

