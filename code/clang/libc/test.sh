#!/bin/bash

BIN="libc-seccomp-filters"
LIBC_DIR="libc"
FILE="syscall.s/open.S"
FILES=$(grep "\"file\":" $LIBC_DIR/compile_commands.json | cut -d':' -f2 | sed 's/^ //;s/"//g')

pushd $LIBC_DIR >/dev/null 2>&1

if [[ $? -ne 0 ]]; then
  echo -e "\033[1;31mNO $LIBC_DIR FOUND\033[0m"
  exit 1
fi

./$BIN $FILE

popd >/dev/null 2>&1
