#!/bin/bash

BIN="libc-seccomp-filters"
# SOURCE="https://downloads.uclibc-ng.org/releases/1.0.30/uClibc-ng-1.0.30.tar.gz"
SOURCE="https://www.fefe.de/dietlibc/dietlibc-0.33.tar.bz2"
SOURCE_TAR="${SOURCE##*/}"
LIBC_DIR="libc"
BUILD_DIR="build"
OUTPUT="output"

RED='\033[1;31m'
RST='\033[0m'

function print_debug () {
  echo -en "${RED}[*] "
  echo -n "$1"
  echo -e "$RST"
}


print_debug "checking: $LIBC_DIR"
if [[ ! -d "$LIBC_DIR" ]]; then
  print_debug "checking: $SOURCE_TAR"
  if [[ ! -f "$SOURCE" ]]; then
    wget "$SOURCE"
  fi
  tar -xjf "$SOURCE_TAR"
  DIR="${SOURCE_TAR%.*}"
  DIR="${DIR%.*}"
  mv "$DIR" "$LIBC_DIR"
  rm "$SOURCE_TAR"
fi

print_debug "building: $BIN"
(cd $BUILD_DIR && make)
# rm $OUTPUT || echo ""

print_debug "checking: $BUILD_DIR/$BIN and $LIBC_DIR/$BIN"
if ! diff $BUILD_DIR/$BIN $LIBC_DIR/$BIN >/dev/null 2>&1; then
  echo "[*] copying..."
  cp $BUILD_DIR/$BIN $LIBC_DIR/$BIN
fi

################################################################################
# To correctly work, use bear (Build EAR) to generate a compile_commands.json.
# Do this by running "bear make"
################################################################################

print_debug "run tool"
pushd $LIBC_DIR >/dev/null 2>&1
echo "[*] $PWD"
LIBC_FILES=$(grep "\"file\":" compile_commands.json | cut -d':' -f2 | sed 's/^ //;s/"//g')
./$BIN $LIBC_FILES
popd >/dev/null 2>&1
