#include "clang/AST/ASTConsumer.h"
#include "clang/AST/Attr.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Lex/MacroArgs.h"
#include "clang/Lex/MacroInfo.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

#include <map>
#include <set>
#include <string>

#define USE_MAP

#ifdef USE_MAP
namespace Info {
std::string current_function;
std::map<std::string, std::set<std::string>> call_map;
} // namespace Info
#endif

namespace Util {
std::string CallExprToString(const clang::CallExpr* CallExpr) {
  // https://jonasdevlieghere.com/understanding-the-clang-ast/#recursiveastvisitor
  if (CallExpr->getType().getTypePtrOrNull() != nullptr) {
    const clang::FunctionDecl* func = CallExpr->getDirectCallee();
    const std::string funcName = func->getNameInfo().getAsString();
    return funcName;
  }
  return nullptr;
}
} // namespace Util

////////////////////////////////////////////////////////////////////////////////
// SYSCALL MACROS
////////////////////////////////////////////////////////////////////////////////
class SyscallMacroCallback : public clang::PPCallbacks {
public:
  explicit SyscallMacroCallback() {}

  void MacroExpands(const clang::Token& MacroNameTok,
                    const clang::MacroDefinition& MD, clang::SourceRange Range,
                    const clang::MacroArgs* Args) {
    clang::IdentifierInfo* idInfo = MacroNameTok.getIdentifierInfo();
    if (!idInfo)
      return;
    std::string idName = idInfo->getName();
    // llvm::outs() << "[*] Macro Name: " << idName << "\n";

    // macro defined as syscall_weak(name,wsym,sym).
    // sym is used by diet-libc, name is the linux syscall,
    // thus mapping sym -> name
    if (idName == "syscall_weak") {
      std::string syscall_name =
          Args->getUnexpArgument(0)->getIdentifierInfo()->getName();
      std::string syscall_alias =
          Args->getUnexpArgument(2)->getIdentifierInfo()->getName();
#ifdef USE_MAP
      // set up circular dependency (both names can be used in the code)
      // TODO: apparently doesn't work for fopen_unlocked <-> fopen
      Info::call_map[syscall_alias].insert(syscall_name);
      Info::call_map[syscall_name].insert(syscall_alias);
#else
      llvm::outs() << "\n[*] Syscall Mapping found: " << syscall_alias << " <-> "
                   << syscall_name << "\n";
#endif
    }
  }

private:
};

////////////////////////////////////////////////////////////////////////////////
// SOURCE CODE VISITOR FOR FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
class LibCVisitor : public clang::RecursiveASTVisitor<LibCVisitor> {
public:
  explicit LibCVisitor(clang::ASTContext* Context) : Context(Context) {}

  bool VisitFunctionDecl(clang::FunctionDecl* FD) {
    if (!Context->getSourceManager().isInMainFile(
            FD->getSourceRange().getBegin()))
      return true;
      // add function name to map
#ifdef USE_MAP
    Info::current_function = FD->getNameAsString();
#else
    llvm::outs() << "\n" << FD->getNameAsString() << ":\n";
#endif

    // check attributes (alias)
    if (FD->hasAttr<clang::AliasAttr>()) {
      clang::AliasAttr* aliasAttr = FD->getAttr<clang::AliasAttr>();
      std::string aliasName = aliasAttr->getAliasee();
#ifdef USE_MAP
      Info::call_map[aliasName].insert(Info::current_function);
#else
      llvm::outs() << aliasName;
#endif
    }

    return true;
  }

  // https://jonasdevlieghere.com/understanding-the-clang-ast/#recursiveastvisitor
  bool VisitCallExpr(clang::CallExpr* CallExpr) {
    if (!Context->getSourceManager().isInMainFile(
            CallExpr->getSourceRange().getBegin()))
      return true;
    if (CallExpr->getType().getTypePtrOrNull() != nullptr) {
      if (CallExpr->getDirectCallee() == nullptr)
        return true;

      const std::string name =
          CallExpr->getDirectCallee()->getNameInfo().getAsString();
#ifdef USE_MAP
      Info::call_map[Info::current_function].insert(name);
#else
      llvm::outs() << name << " ";
#endif
    }
    return true;
  }

private:
  clang::ASTContext* Context;
};

class LibCConsumer : public clang::ASTConsumer {
public:
  explicit LibCConsumer(clang::ASTContext* Context) : Visitor(Context) {}

  virtual void HandleTranslationUnit(clang::ASTContext& Context) {
    Visitor.TraverseDecl(Context.getTranslationUnitDecl());
  }

private:
  LibCVisitor Visitor;
};

class LibCAction : public clang::ASTFrontendAction {
public:
  std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance& Compiler,
                    llvm::StringRef /* InFile */) override {
    return std::unique_ptr<clang::ASTConsumer>(
        new LibCConsumer(&Compiler.getASTContext()));
  }

  bool BeginSourceFileAction(clang::CompilerInstance& CI) override {
    auto callback = std::make_unique<SyscallMacroCallback>();
    CI.getPreprocessor().addPPCallbacks(std::move(callback));
    return true;
  }
};

class LibCToolFactory : public clang::tooling::FrontendActionFactory {
  clang::FrontendAction* create() override { return new LibCAction; }
};

static llvm::cl::OptionCategory
    LibCOptions("Generate the Seccomp Filters for the Auto-Seccomp Plugin");
static llvm::cl::extrahelp
    CommonHelp(clang::tooling::CommonOptionsParser::HelpMessage);
static llvm::cl::extrahelp MoreHelp("\nMore help text...\n");

int main(int argc, const char** argv) {
  using namespace clang::tooling;
  CommonOptionsParser OptionsParser(argc, argv, LibCOptions);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());
  auto rv = Tool.run(new LibCToolFactory());
#ifdef USE_MAP
  // print the call map we created
  llvm::outs() << "// ########### RESULT ########### //\n{\n";
  // print the function
  for (const auto& entry : Info::call_map) {
    const auto& set = entry.second;
    llvm::outs() << "{\"" << entry.first << "\", { ";
    // print all used calls
    for (const auto& call : set)
      llvm::outs() << "\"" << call << "\", ";
    llvm::outs() << " }},\n";
  }
  llvm::outs() << "}\n";
#endif
  return rv;
}
