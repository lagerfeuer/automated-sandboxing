cmake_minimum_required(VERSION 3.11)
# set(CMAKE_VERBOSE_MAKEFILE on)

set(NAME auto-seccomp)
project(${NAME})

# set(CMAKE_CXX_STANDARD 14)

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
add_subdirectory(source)
