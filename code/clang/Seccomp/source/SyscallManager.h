#pragma once
#include <map>
#include <set>
#include <string>
#include <vector>

#define INIT_SECCOMP_SET

class SyscallManager {
private:
  // syscall list
  static const std::vector<std::string> SYSCALL_NAMES;
  // libc mapping of functions to used syscalls
  static const std::map<std::string, std::set<std::string>>
      LIBC_FUNCTION_SYSCALL_MAP;
  // final set of seccomp rules
  std::set<std::string> seccomp_set;

  SyscallManager();
  static SyscallManager* instance_;

  std::string SOURCE_DIR;
  // constants
  const std::string FILE_NAME = "auto-seccomp";
  const std::string FUNCTION_NAME = "auto_seccomp_setup";

public:
  static SyscallManager* instance();

  // Seccomp Checks
  static bool isSyscall(const std::string& name);

  // Seccomp Filters
  void addToSeccomp(std::string name);
  std::string getSeccompFilters() const;
  // LibC
  static bool isLibCCall(const std::string& name);
  std::set<std::string>
  getUsedSyscallsForLibCFunction(const std::string& libc_function_name, std::set<std::string>* excluded = nullptr);
  // Seccomp Filters for File
  int dumpSeccompFiltersToFile() const;

  // Seccomp Names/Files/Calls to insert
  const std::string getSeccompSetupFunctionCall() const;
  const std::string getSeccompSetupInclude() const;

  // Getters and Setters
  const std::string getFileName() const;
  const std::string getHeaderFileName() const;
  const std::string getFunctionName() const;
  const std::string getSourceDir() const;
  void setSourceDir(const std::string& source_dir);
};
