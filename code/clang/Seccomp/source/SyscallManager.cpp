#include "SyscallManager.h"
#include "llvm/Support/raw_ostream.h"
#include <algorithm>
#include <fstream>
#include <sstream>

SyscallManager* SyscallManager::instance_ = nullptr;

SyscallManager::SyscallManager()
    :
#ifdef INIT_SECCOMP_SET
      seccomp_set {
  "write", "read", "rt_sigreturn", "exit", "exit_group"
}
#else
      seccomp_set()
#endif
, SOURCE_DIR("") {}

SyscallManager* SyscallManager::instance() {
  if (instance_ == nullptr)
    instance_ = new SyscallManager();
  return instance_;
}

bool SyscallManager::isSyscall(const std::string& name) {
  return std::find(SYSCALL_NAMES.begin(), SYSCALL_NAMES.end(), name) !=
         SYSCALL_NAMES.end();
}

bool SyscallManager::isLibCCall(const std::string& name) {
  return (LIBC_FUNCTION_SYSCALL_MAP.find(name) !=
          LIBC_FUNCTION_SYSCALL_MAP.end());
}

void SyscallManager::addToSeccomp(std::string name) {
  seccomp_set.insert(name);
}

std::string SyscallManager::getSeccompFilters() const {
  std::stringstream seccomp_filters_sstr;
  seccomp_filters_sstr
      << "scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL);\n";
  for (const std::string& syscall : seccomp_set)
    seccomp_filters_sstr << "seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS("
                         << syscall << "), 0);\n";
  seccomp_filters_sstr << "seccomp_load(ctx);\n";
  // TODO do we have to include this line?
  seccomp_filters_sstr << "seccomp_release(ctx);\n";
  return seccomp_filters_sstr.str();
}
int SyscallManager::dumpSeccompFiltersToFile() const {
  std::ofstream header_file(SOURCE_DIR + "/" + FILE_NAME + ".h");
  header_file << "void " << FUNCTION_NAME << "();\n";
  header_file.close();
  std::ofstream source_file(SOURCE_DIR + "/" + FILE_NAME + ".c");
  source_file << "#include \"" << FILE_NAME << ".h\"\n"
              << "#include <seccomp.h>\n\n"
              // function implementation
              << "void " << FUNCTION_NAME << "() {\n"
              << getSeccompFilters() << "\n}\n";
  source_file.close();
  return 0;
}

std::set<std::string> SyscallManager::getUsedSyscallsForLibCFunction(
    const std::string& libc_function_name, std::set<std::string>* excluded) {
  std::set<std::string> used_syscalls;
  std::set<std::string> seen_functions;
  if (excluded != nullptr)
    seen_functions = *excluded;

  if (!isLibCCall(libc_function_name))
    return used_syscalls;
  for (const auto& call_name :
       LIBC_FUNCTION_SYSCALL_MAP.at(libc_function_name)) {
    if (isSyscall(call_name))
      used_syscalls.insert(call_name);
    if (seen_functions.find(call_name) == seen_functions.end()) {
      seen_functions.insert(call_name);
      std::set<std::string> tmp_call_set =
          getUsedSyscallsForLibCFunction(call_name, &seen_functions);
      // merge the result of the recursive call with our set
      used_syscalls.insert(tmp_call_set.begin(), tmp_call_set.end());
    }
  }

  return used_syscalls;
}

const std::string SyscallManager::getFileName() const { return FILE_NAME; }

const std::string SyscallManager::getHeaderFileName() const {
  return FILE_NAME + ".h";
}

const std::string SyscallManager::getFunctionName() const {
  return FUNCTION_NAME;
}

const std::string SyscallManager::getSourceDir() const { return SOURCE_DIR; }
void SyscallManager::setSourceDir(const std::string& source_dir) {
  SOURCE_DIR = source_dir;
}

const std::string SyscallManager::getSeccompSetupFunctionCall() const {
  return "\n// AUTO GENERATED\n" + FUNCTION_NAME + "();\n";
}

const std::string SyscallManager::getSeccompSetupInclude() const {
  return "#include \"" + FILE_NAME + ".h\"";
}
