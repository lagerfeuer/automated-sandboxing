#pragma once

#include <clang/AST/Expr.h>
#include <string>

namespace CMD {
static std::string RED = "\033[1;31m";
static std::string GREEN = "\033[1;32m";
static std::string RESET = "\033[0m";
} // namespace CMD

namespace Utils {
std::string CallExprToString(const clang::CallExpr* CallExpr);
}
