#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Basic/Module.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TokenKinds.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Lex/Lexer.h"
#include "clang/Lex/PPCallbacks.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Rewrite/Core/RewriteBuffer.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

#include "llvm/ADT/StringRef.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"

#include "SyscallManager.h"
#include "Utils.h"

#ifdef USE_MATCHERS
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#endif

using clang::dyn_cast;
using clang::isa;

//******************************************************************************
// COMMAND LINE OPTIONS
//******************************************************************************
namespace {
static llvm::cl::OptionCategory SeccompCategory("Seccomp Filter Options");
static llvm::cl::extrahelp SeccompCategoryHelp(R"(
Generate Seccomp Filter from used Syscalls automatically.
)");
static llvm::cl::opt<bool> PrintSyscallsOption(
    "print-syscalls", llvm::cl::desc("Print syscalls using Clang diagnostics"),
    llvm::cl::cat(SeccompCategory));
static llvm::cl::opt<bool> DryRunOption(
    "dry-run",
    llvm::cl::desc(
        "Do not change files or write the filter, only output to terminal"),
    llvm::cl::cat(SeccompCategory));
static llvm::cl::extrahelp
    CommonHelp(clang::tooling::CommonOptionsParser::HelpMessage);
static llvm::cl::extrahelp MoreHelp("\nVersion: Static Lookup\n");
} // namespace

//******************************************************************************
// INFORMATION STORAGE
//******************************************************************************
namespace SeccompInfo {
bool insert = false;
namespace Include {
bool found = false;
bool locationSet = false;
clang::SourceLocation Location;
} // namespace Include
namespace FunctionCall {
bool found = false;
static const std::string BIND_NAME = "auto-seccomp-setup-call";
} // namespace FunctionCall
} // namespace SeccompInfo

//******************************************************************************
// INCLUDE ANALYZER AND REWRITE
//******************************************************************************
class IncludeCallback : public clang::PPCallbacks {
public:
  explicit IncludeCallback(clang::Rewriter& Rewriter) : Rewriter(Rewriter) {}

  void InclusionDirective(clang::SourceLocation HashLoc,
                          const clang::Token& IncludeTok,
                          llvm::StringRef FileName, bool IsAngled,
                          clang::CharSourceRange FilenameRange,
                          const clang::FileEntry* File,
                          llvm::StringRef SearchPath,
                          llvm::StringRef RelativePath,
                          const clang::Module* Imported,
                          clang::SrcMgr::CharacteristicKind FileType) override {
    if (!Rewriter.getSourceMgr().isInMainFile(HashLoc))
      return;

    if (FileName == SyscallManager::instance()->getHeaderFileName())
      SeccompInfo::Include::found = true;
    SeccompInfo::Include::locationSet = true;
    SeccompInfo::Include::Location = FilenameRange.getEnd();
  }

private:
  clang::Rewriter& Rewriter;
};

//******************************************************************************
// SOURCE CODE ANALYZER AND MAIN REWRITE
// https://clang.llvm.org/docs/RAVFrontendAction.html
//******************************************************************************
class SeccompVisitor : public clang::RecursiveASTVisitor<SeccompVisitor> {
public:
  explicit SeccompVisitor(clang::ASTContext* Context, clang::Rewriter& Rewriter)
      : Context(Context), Rewriter(Rewriter) {}

  bool VisitFunctionDecl(clang::FunctionDecl* FD) {
    if (!Context->getSourceManager().isInMainFile(
            FD->getSourceRange().getBegin()))
      return true;

    // only enter when we have the main function
    // TODO: use isMain() provided by the framework
    // https://clang.llvm.org/doxygen/classclang_1_1FunctionDecl.html#aa2b31caf653741632b16cce1ae2061cc
    if (FD->hasBody() && FD->isMain()
        // isa<clang::CompoundStmt>(FD->getBody()) &&
        // FD->getNameAsString() == "main"
    ) {
      SeccompInfo::insert = true;
      const auto compoundStmt = dyn_cast<clang::CompoundStmt>(FD->getBody());
      const auto seccompLoc = compoundStmt->getLBracLoc().getLocWithOffset(1);

      const auto firstStmt = compoundStmt->child_begin();
      if (firstStmt != compoundStmt->child_end())
        SeccompInfo::FunctionCall::found =
            (Utils::CallExprToString(dyn_cast<clang::CallExpr>(*firstStmt)) ==
             SyscallManager::instance()->getFunctionName());

      // if there are no includes, we need to define the include location as
      // "before main"
      // TODO: can this ever happen?
      if (!SeccompInfo::Include::found && !SeccompInfo::Include::locationSet)
        SeccompInfo::Include::Location = FD->getSourceRange().getBegin();

      // only insert if we haven't found a call to the setup function
      if (!SeccompInfo::FunctionCall::found)
        Rewriter.InsertText(
            seccompLoc,
            SyscallManager::instance()->getSeccompSetupFunctionCall(), true,
            true);

      const auto fileEntry = Context->getSourceManager().getFileEntryForID(
          Context->getSourceManager().getMainFileID());
      SyscallManager::instance()->setSourceDir(fileEntry->getDir()->getName());
    }
    return true;
  }

  // https://jonasdevlieghere.com/understanding-the-clang-ast/#recursiveastvisitor
  bool VisitCallExpr(clang::CallExpr* CE) {

    if (CE == nullptr)
      return true;

    clang::QualType qt = CE->getType();
    const clang::Type* type = qt.getTypePtrOrNull();
    if (type == nullptr)
      return true;

    // FD can be null, e.g. a function pointer gets passed as argument and
    // called
    clang::FunctionDecl* FD = CE->getDirectCallee();
    if (FD == nullptr)
      return true;

    std::string func_name = FD->getNameAsString();

    // check if call is seccomp setup function
    if (func_name == SyscallManager::instance()->getFunctionName())
      SeccompInfo::FunctionCall::found = true;

    // === DIAGNOSTICS ===
    const clang::SourceLocation loc =
        Context->getSourceManager().getFileLoc(CE->getLocStart());
    clang::DiagnosticsEngine& Engine = Context->getDiagnostics();

    // TODO: if syscall or libc call has been processed before (first
    // occurance), skip this and gain better performance
    if (SyscallManager::isSyscall(func_name)) {
      // check if function call is call to syscall
      SyscallManager::instance()->addToSeccomp(func_name);
      if (PrintSyscallsOption) {
        // get correct location (.c file, not header definition)
        const unsigned ID = Engine.getCustomDiagID(
            clang::DiagnosticsEngine::Remark, "Found Syscall '%0'");
        clang::DiagnosticBuilder DB = Engine.Report(loc, ID);
        DB.AddString(func_name);
      }
    }
    if (SyscallManager::isLibCCall(func_name)) {
      // check if function call is to libc, if yes, check if we need to add
      // syscalls to the filter set
      // llvm::outs() << "[*] Checking " << func_name << "\n";
      for (const std::string& syscall :
           SyscallManager::instance()->getUsedSyscallsForLibCFunction(
               func_name)) {
        SyscallManager::instance()->addToSeccomp(syscall);
        if (PrintSyscallsOption) {
          // get correct location (.c file, not header definition)
          const unsigned ID = Engine.getCustomDiagID(
              clang::DiagnosticsEngine::Remark,
              "Found Syscall '%0' in libC function '%1'");
          clang::DiagnosticBuilder DB = Engine.Report(loc, ID);
          DB.AddString(syscall);
          DB.AddString(func_name);
        }
      }
    }
    return true;
  }

private:
  clang::ASTContext* Context;
  clang::Rewriter& Rewriter;
};

#ifdef USE_MATCHERS
//******************************************************************************
// COMBINING VISITOR AND MATCHER
//******************************************************************************
class FinderCallback : public clang::ast_matchers::MatchFinder::MatchCallback {
public:
  void
  run(const clang::ast_matchers::MatchFinder::MatchResult& Result) override {
    using namespace clang::ast_matchers;
    // found matching call to setup function
    if (/* const clang::CallExpr* match = */ Result.Nodes
            .getNodeAs<clang::CallExpr>(SeccompInfo::FunctionCall::BIND_NAME)) {
      SeccompInfo::FunctionCall::found = true;
    }
  }
};
#endif

//******************************************************************************
// COMBINING VISITOR AND MATCHER
//******************************************************************************
class SeccompConsumer : public clang::ASTConsumer {
public:
  explicit SeccompConsumer(clang::ASTContext* Context,
                           clang::Rewriter& Rewriter)
      : Visitor(Context, Rewriter) {}

  virtual void HandleTranslationUnit(clang::ASTContext& Context) {
#ifdef USE_MATCHERS
    // check if function call already in main
    {
      using namespace clang::ast_matchers;
      // clang-format off
      const auto AutoSeccompSetupMatcher =
        callExpr(
            hasAncestor(functionDecl(hasName("main"))),
            callee(functionDecl(hasName(
              SyscallManager::instance()->getFunctionName()
            )))
        ).bind(SeccompInfo::FunctionCall::BIND_NAME);
      // clang-format on
      FinderCallback Callback;
      MatchFinder Finder;
      Finder.addMatcher(AutoSeccompSetupMatcher, &Callback);
      Finder.matchAST(Context);
    }
#endif

    // now, if we find the main function, we know whether to add something or if
    // the call is already in there
    Visitor.TraverseDecl(Context.getTranslationUnitDecl());
  }

private:
  SeccompVisitor Visitor;
};

//******************************************************************************
// COMBINING SOURCE CODE AND INCLUDE ANALYZER INTO SINGLE ACTION
//******************************************************************************
class SeccompAction : public clang::ASTFrontendAction {
public:
  std::unique_ptr<clang::ASTConsumer>
  CreateASTConsumer(clang::CompilerInstance& CI,
                    llvm::StringRef FileName) override {
    this->FileName = FileName.data();
    return std::unique_ptr<clang::ASTConsumer>(
        new SeccompConsumer(&CI.getASTContext(), Rewriter));
  }

  bool BeginInvocation(clang::CompilerInstance& CI) override {
    // reset SeccompInfo::insert for every file, otherwise we include the
    // seccomp filters everywhere
    SeccompInfo::insert = false;
    SeccompInfo::Include::found = false;
    SeccompInfo::FunctionCall::found = false;
    Rewriter.setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
    return true;
  }

  bool BeginSourceFileAction(clang::CompilerInstance& CI) override {
    auto callback = std::make_unique<IncludeCallback>(Rewriter);
    CI.getPreprocessor().addPPCallbacks(std::move(callback));
    return true;
  }

  void EndSourceFileAction() override {
    if (SeccompInfo::insert && !SeccompInfo::Include::found)
      Rewriter.InsertText(
          SeccompInfo::Include::Location,
          "\n" + SyscallManager::instance()->getSeccompSetupInclude() + "\n");

    const auto rewriteBuffer =
        Rewriter.getRewriteBufferFor(Rewriter.getSourceMgr().getMainFileID());

    if (DryRunOption && rewriteBuffer) {
      llvm::outs() << CMD::GREEN << "*** " << FileName << " ***" << CMD::RESET
                   << "\n";
      rewriteBuffer->write(llvm::outs());
      llvm::outs() << "\n";
    } else
      Rewriter.overwriteChangedFiles(); // write changes to file
  }

private:
  std::string FileName;
  clang::Rewriter Rewriter;
};

class SeccompToolFactory : public clang::tooling::FrontendActionFactory {
  clang::FrontendAction* create() override { return new SeccompAction; }
};

int main(int argc, const char** argv) {
  using namespace clang::tooling;

  CommonOptionsParser OptionsParser(argc, argv, SeccompCategory);
  ClangTool Tool(OptionsParser.getCompilations(),
                 OptionsParser.getSourcePathList());

  int result = Tool.run(new SeccompToolFactory());

  if (DryRunOption) {
    llvm::outs() << CMD::RED << "*** Seccomp Filters ***" << CMD::RESET << "\n";
    llvm::outs() << SyscallManager::instance()->getSeccompFilters();
  } else
    SyscallManager::instance()->dumpSeccompFiltersToFile();

  return result;
}
