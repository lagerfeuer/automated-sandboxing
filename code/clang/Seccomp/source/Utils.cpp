#include "Utils.h"
#include <clang/AST/Decl.h>

namespace Utils {
std::string CallExprToString(const clang::CallExpr* CallExpr) {
  // https://jonasdevlieghere.com/understanding-the-clang-ast/#recursiveastvisitor
  if (CallExpr != nullptr &&
      CallExpr->getType().getTypePtrOrNull() != nullptr) {
    const clang::FunctionDecl* func = CallExpr->getDirectCallee();
    const std::string funcName = func->getNameInfo().getAsString();
    return funcName;
  }
  return "[error]";
}
} // namespace Utils
