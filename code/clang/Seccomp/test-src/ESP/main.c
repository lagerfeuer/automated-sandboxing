//-----------------------------------------------------------------------------
// assb.c
//
// Turing Machine
//
// Group: 13490 David Bidner
//
// Authors: Lukas Deutz 1531144
//          Johannes Erlacher 1530662
//          Nico Nössler 1431141
//
// Latest Changes: 17.02.2016 (by Lukas Deutz)
//-----------------------------------------------------------------------------
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INIT -1

#define EOL         '\n'
#define PROMPT      "esp> "
#define SUCCESS     0
#define ERR_USAGE   1
#define ERR_MEMORY  2
#define ERR_INPUT   3
#define ERR_FILE    4
#define ERR_NODMTM  5
#define QUIT        6

#define BAND_WIDTH  512
#define RULES_BLOCK 8

#define OK        0
#define NO_INPUT  1
#define TOO_LONG  2

#define FALSE     0
#define TRUE      1

#define NO_MATCH    0
#define MATCH       1

typedef enum _Mode_ {STATE, WRITE, READ, POS} Mode;

typedef union _Condition_
{
  int number;
  char sign;
} Condition;

typedef struct _Rule_
{
  int state_in_;
  int state_out_;
  char sign_in_;
  char sign_out_;
  char direction_;
} Rule;

typedef struct _Breakpoint_
{
  Mode mode_;
  Condition con_;
  struct _Breakpoint_ *next_;
  int band_cnt_;
} Breakpoint;

typedef struct _Turing_
{
  signed int pos_;
  int state_;
  int band_cnt_;
  char *band_;
  int executed_;
  int rules_length_;
  int rules_block_cnt_;
  int next_rule_;
  Rule **rules_;
  int breakpoint_length_;
  Breakpoint *breakpoint_;
} Turing;

int getPosition(Turing *turing, signed int pos, int band_cnt);
int reallocBand(Turing *turing, int parse);
void init(Turing *turing);
int parseRules(FILE *file, Turing *turing);
int checkParse(Turing *turing);
int execMachine(Turing *turing, int step);
void freeMem(Turing *turing);
int interactiveShell(Turing *turing);
int getNextRule(Turing *turing);
void listTuring(Turing *turing);
void showTuring(Turing *turing);
int addBreakpoint(Turing *turing, Mode mode, Condition con, int band_cnt);
int checkBreakpoint(Turing *turing, int curr_rule, int first_run);
void printRule(Turing *turing, int rule_num);
int errorHandler(Turing *turing, int returned_value, int freeMemory);
static int getLine (char *prmpt, char *buff, size_t sz);

//-----------------------------------------------------------------------------
///
/// main
///
/// @param argc argument count
/// @param argv arguments
///
/// @return values depending on errors (further information -> #define ERR_XXX)
//
int main(int argc, char *argv[]) {
  FILE* file = NULL;
  Turing *turing = NULL;
  int returned_value;

  if(argc == 1)
    return errorHandler(turing, ERR_USAGE, FALSE);

  if (!(file = fopen(argv[1], "r")))
    return errorHandler(turing, ERR_FILE, FALSE);

  turing = (Turing*) malloc(sizeof(Turing));
  if(!turing)
    return errorHandler(turing, ERR_MEMORY, FALSE);
  init(turing);

  returned_value = parseRules(file, turing);
  fclose(file);
  if(returned_value != SUCCESS)
    return errorHandler(turing, returned_value, TRUE);

  returned_value = checkParse(turing);
  if(returned_value != SUCCESS)
    return errorHandler(turing, returned_value, TRUE);
  
  returned_value = interactiveShell(turing);
  if((returned_value != SUCCESS) && (returned_value != QUIT))
    return errorHandler(turing, returned_value, TRUE);
  else
    freeMem(turing);
  return SUCCESS;
}

//-----------------------------------------------------------------------------
///
/// freeMem free all allocated memory
///
/// @param turing  turing machine
//
void freeMem(Turing *turing)
{
  free(turing->band_);
  int cnt;
  if(turing->rules_length_ != INIT)
  {
    for(cnt = 0; cnt <= turing->rules_length_; cnt++)
    {
      free(turing->rules_[cnt]);
    }
  }
  free(turing->rules_);
  Breakpoint *next, *curr_bp = turing->breakpoint_;
  while(curr_bp)
  {
    next = curr_bp->next_;
    free(curr_bp);
    curr_bp = next;
  }
  free(turing);
}

//-----------------------------------------------------------------------------
///
/// init
///
/// @param turing  turing machine
//
void init(Turing *turing)
{
  turing->band_cnt_ = 1;
  turing->rules_block_cnt_ = 1;
  turing->rules_length_ = INIT;
  turing->next_rule_ = INIT;
  turing->breakpoint_length_ = 0;
  turing->breakpoint_ = 0;
  turing->executed_ = FALSE;
}

//-----------------------------------------------------------------------------
///
/// parseRules
///
/// @param file    filestream of Ruleset
/// @param turing  turing machine which will be created
///
/// @return values depending on errors (further information -> #define ERR_XXX)
//
int parseRules(FILE *file, Turing *turing)
{
  char *first_row = NULL;
  int lineRead = FALSE;
  int init, cnt;
  int finished = FALSE;

  turing->band_ = (char*) calloc(BAND_WIDTH *
    turing->band_cnt_, sizeof(char)); 

  turing->rules_ = malloc(RULES_BLOCK * turing->rules_block_cnt_
    * sizeof(Rule*));

  first_row = (char*) malloc(BAND_WIDTH / 2 * sizeof(char));

  if(!turing->band_ || !turing->rules_ || !first_row)
  {
    free(first_row);
    return ERR_MEMORY;
  }
  while(!lineRead)
  {
    if(fgets(first_row, BAND_WIDTH / 2 * turing->band_cnt_, file) == NULL)
    {
      free(first_row);
      return ERR_INPUT;
    }
    for (cnt = 0; cnt < (BAND_WIDTH / 2 * turing->band_cnt_); cnt++)
    {
      if(first_row[cnt] == '\n')
      {
        first_row[cnt] = '\0';
        lineRead = TRUE;
        break;
      }
    } 
    if(!lineRead)
    {
      rewind(file);
      reallocBand(turing, FALSE);
      char *tmp = (char*) realloc(first_row, BAND_WIDTH * turing->band_cnt_ / 
        2 * sizeof(char));
      if(tmp)
        first_row = tmp;
      else
      {
        free(first_row);
        return ERR_MEMORY;
      }
    }
  }
  
  sscanf(first_row, "%s", turing->band_ + 
    (BAND_WIDTH / 2 * turing->band_cnt_));
  free(first_row);

  for(init = 0; init < BAND_WIDTH * turing->band_cnt_; init++)
  {
    if(turing->band_[init] == 0)    
      turing->band_[init] = '_';
  }

  if(fscanf(file, " %d", &turing->pos_) != 1)
    return ERR_INPUT;

  while(!finished)
  {
    if(((BAND_WIDTH / 2 * turing->band_cnt_) > turing->pos_) && 
      (((-1) * BAND_WIDTH / 2 * turing->band_cnt_) <= turing->pos_))
      break;
    
    if(reallocBand(turing, TRUE))
      return ERR_MEMORY;
    
  }
  if(fscanf(file, " %d", &turing->state_) != 1)
    return ERR_INPUT;

  turing->pos_ = getPosition(turing, turing->pos_, turing->band_cnt_);

  turing->rules_length_++;

  while(turing->rules_length_ <= RULES_BLOCK * turing->rules_block_cnt_)
  {
    if(turing->rules_length_ == RULES_BLOCK * turing->rules_block_cnt_)
    {
      Rule **tmp = NULL;
      tmp = realloc(turing->rules_, RULES_BLOCK *
        ++turing->rules_block_cnt_ * sizeof(Rule*));
      if(tmp)
      {
        turing->rules_ = tmp;
        tmp = 0;
      }
      else
      {
        turing->rules_length_--;
        return ERR_MEMORY;
      }
    }
    turing->rules_[turing->rules_length_] = (Rule*) malloc(sizeof(Rule));

    if(!turing->rules_[turing->rules_length_])
      return ERR_MEMORY;

    int return_val = fscanf(file, " %d %c %c %d %c",
      &turing->rules_[turing->rules_length_]->state_in_,
      &turing->rules_[turing->rules_length_]->sign_in_,
      &turing->rules_[turing->rules_length_]->sign_out_,
      &turing->rules_[turing->rules_length_]->state_out_,
      &turing->rules_[turing->rules_length_]->direction_);
    if(return_val == -1)
      return SUCCESS;
    else if(return_val == 5)
      turing->rules_length_++;
    else
      return ERR_INPUT;
  }
  return SUCCESS;
}


//-----------------------------------------------------------------------------
///
/// checkParse
///
/// @param turing parameters including ruletabel
///
/// @return checkvalue 0 = ok, 1 = error
//
int checkParse(Turing *turing)
{
  int state_to_check;
  int state_count;
  int curr_in_state;
  char curr_in_sign;

  for(state_to_check = 0; state_to_check < turing->rules_length_;
    state_to_check++)
  {
    for(state_count = 1; state_count < turing->rules_length_;
      state_count++)
    {
      if(state_count == state_to_check)
      {
        continue;
      }
      curr_in_state = turing->rules_[state_to_check]->state_in_;
      curr_in_sign = turing->rules_[state_to_check]->sign_in_;
      if(curr_in_state == turing->rules_[state_count]->state_in_ &&
        curr_in_sign == turing->rules_[state_count]->sign_in_)
        return ERR_NODMTM;
    }
  }
  return SUCCESS;
}

//-----------------------------------------------------------------------------
///
/// execMachine
///
/// @param turing   Machine which will be executed
/// @param step     for step mode (TRUE/FALSE)
///
/// @return values depending on errors (further information -> #define ERR_XXX)
//
int execMachine(Turing *turing, int step)
{
  int curr_state = turing->state_;
  int fine_rule;
  int stepped = FALSE;
  int first_run = TRUE;
  int end = FALSE;

  while(end == FALSE && stepped == FALSE && turing->executed_ == FALSE)
  {
    fine_rule = getNextRule(turing);
    if(fine_rule != INIT)
    {
      if(checkBreakpoint(turing, fine_rule, first_run))
        break;

      turing->band_[turing->pos_] = turing->rules_[fine_rule]->sign_out_;

      if(turing->rules_[fine_rule]->direction_ == 'R')
      {
        if((turing->pos_ + 1) > (BAND_WIDTH * turing->band_cnt_) - 2)
        {
          if(reallocBand(turing, FALSE))
            return ERR_MEMORY;
        }
        turing->pos_++;
      }
      if(turing->rules_[fine_rule]->direction_ == 'L')
      {
        if((turing->pos_ - 1) < 0)
        {
          if(reallocBand(turing, FALSE))
            return ERR_MEMORY;
        }
        turing->pos_--;
      }

      if(step == TRUE)
      {
        printRule(turing, fine_rule);
        stepped = TRUE;
      }

      curr_state = turing->rules_[fine_rule]->state_out_;
      turing->state_ = turing->rules_[fine_rule]->state_out_;
    }
    else if(fine_rule == -1)
    {
      end = TRUE;
    }
    first_run = FALSE;
  }

  if(end == TRUE)
  {
    printf("machine stopped in state %d\n", curr_state);
    turing->executed_ = TRUE;
    turing->state_ = curr_state;
    showTuring(turing);
    return QUIT;
  }
  return SUCCESS;
}


//-----------------------------------------------------------------------------
///
/// interactive
///
/// @param turing   Machine which will be executed
///
/// @return values depending on errors (further information -> #define ERR_XXX)
//
int interactiveShell(Turing *turing)
{
  int running = 1;
  char input[100];
  char subbuff[20];
  char commands[3][6];
  int break_con;
  char break_sign;
  int add_break = FALSE;
  int returned_value;
  int searching = TRUE;
  int band_cnt;
  Mode mode = INIT;
  Condition condition;

  if(getNextRule(turing) == INIT)
    return execMachine(turing, FALSE);

  while(running)
  {
    band_cnt = turing->band_cnt_;
    returned_value = getLine(PROMPT, input, sizeof(input));
    if(returned_value == QUIT)
    {
      printf("Bye.\n");
      return SUCCESS;
    }
    if(!returned_value)
    {
      memcpy(subbuff, input, 4);
      if(subbuff[0] == EOF){
        printf("Bye.\n");
        return SUCCESS;
      }
      subbuff[4] = '\0';
      if(strcmp(subbuff, "quit") == 0)
      {
        printf("Bye.\n");
        return SUCCESS;
      }

      if(strcmp(subbuff, "list") == 0)
      {
        listTuring(turing);
        continue;
      }

      if(strcmp(subbuff, "show") == 0)
      {
        showTuring(turing);
        continue;
      }

      if(strcmp(subbuff, "step") == 0)
      {
        returned_value = execMachine(turing, TRUE);
        if(returned_value == SUCCESS)
          continue;
        else if(returned_value == QUIT)
          return SUCCESS;
        else
          return returned_value;
      }

      memcpy(subbuff, input, 8);
      subbuff[8] = '\0';
      if(strcmp(subbuff, "continue") == 0)
      {
        returned_value = execMachine(turing, FALSE);
        if(returned_value == SUCCESS)
          continue;
        else if(returned_value == QUIT)
          return SUCCESS;
        else
          return returned_value;
      }

      memcpy(subbuff, input, 19);
      subbuff[19] = '\0';

      sscanf(subbuff, "%s %s %s ", commands[0], commands[1], commands[2]);
      if((strcmp(commands[0], "break")) == 0)
      {
        add_break = TRUE;
        if(!(strcmp(commands[1], "state")) || !(strcmp(commands[1], "pos")))
        {
          mode = (!strcmp(commands[1], "state")) ? STATE : POS;
          if(sscanf(commands[2], "%d", &break_con) != 1)          
            add_break = FALSE;
          if(mode == POS)
          {
            while(searching)
            {
              if(break_con < (BAND_WIDTH / 2 * band_cnt) &&
                break_con >= (-1) * BAND_WIDTH / 2 * band_cnt)
              {
                condition.number = getPosition(turing, break_con, band_cnt);
                break;
              }
              band_cnt++;
            }
          }
          else
          {
            condition.number = break_con;
          }
        }
        if(!(strcmp(commands[1], "write")) || !(strcmp(commands[1], "read")))
        {
          mode = (!strcmp(commands[1], "write")) ? WRITE : READ;
          if(sscanf(commands[2], "%c", &break_sign) != 1)
            add_break = FALSE;
          condition.sign = break_sign;
        }
        if(add_break && (mode != INIT))
          addBreakpoint(turing, mode, condition, band_cnt);
      }
    }
  }
  
  return SUCCESS;
}

//-----------------------------------------------------------------------------
///
/// listTuring
///
/// @param turing   Machine which is executed
//
void listTuring(Turing *turing)
{
  int next_rule = getNextRule(turing);
  int cnt;
  for (cnt = 0; cnt < turing->rules_length_; cnt++)
  {
      if(cnt == next_rule)
        printf(">>> ");
      printRule(turing, cnt);
  }
}

//-----------------------------------------------------------------------------
///
/// showTuring
///
/// @param turing   Machine which is executed
//
void showTuring(Turing *turing)
{
  int search;
  int print;
  int found_first = FALSE;
  int first_pos = 0;
  int last_pos = 0;

  for(search = 0; search <= (BAND_WIDTH * turing->band_cnt_) - 1; search++)
  {
    if(turing->band_[search] != '_')
    {
      last_pos = search;

      if(found_first == FALSE)
      {
        first_pos = search;
        found_first = TRUE;
      }
    }
  }

  if(turing->pos_ < first_pos)
    first_pos = turing->pos_;
  if(turing->pos_ > last_pos)
    last_pos = turing->pos_;

  for(print = first_pos; print <= (last_pos - 1); print++)
  {
    if(print == turing->pos_)
      printf(">%c<|", turing->band_[print]);
    else
      printf("%c|", turing->band_[print]);
  }

  if(last_pos == turing->pos_)
    printf(">%c<\n", turing->band_[last_pos]);
  else
    printf("%c\n", turing->band_[last_pos]);
}

//-----------------------------------------------------------------------------
///
/// getNextRule
///
/// @param turing   Machine which is executed
///
/// @return next rule
//
int getNextRule(Turing *turing)
{
  int state = turing->state_;
  char sign = turing->band_[turing->pos_];
  int curr_rule;

  for(curr_rule = 0; curr_rule < turing->rules_length_; curr_rule++)
  {
    if(turing->rules_[curr_rule]->state_in_ == state && 
      turing->rules_[curr_rule]->sign_in_ == sign)
      return curr_rule;
  }
  
  return INIT;
}

//-----------------------------------------------------------------------------
///
/// addBreakpoint
///
/// @param turing   Machine which is executed
/// @param mode     Mode/Type of breakpoint
/// @param data     state/read or write sign/position
///
/// @return 0 on success, ERR_MEMORY on failure
//
int addBreakpoint(Turing *turing, Mode mode, Condition con, int band_cnt)
{
  Breakpoint *breakpoint = (Breakpoint*) malloc(sizeof(Breakpoint));
  if(!breakpoint)
    return ERR_MEMORY;

  breakpoint->mode_ = mode;
  breakpoint->con_ = con;
  breakpoint->band_cnt_ = band_cnt;
  breakpoint->next_ = 0;

  if(!turing->breakpoint_length_)
  {
    turing->breakpoint_ = breakpoint;
  }
  else
  {
    Breakpoint *curr_bp = turing->breakpoint_;
    while(curr_bp->next_)
    {
      curr_bp = curr_bp->next_;
    }
    curr_bp->next_ = breakpoint;
  }
  turing->breakpoint_length_++;
  
  return SUCCESS;
}

//-----------------------------------------------------------------------------
///
/// checkBreakpoint
///
/// @param turing   Machine which is executed
/// @param curr_rule current rule executed
///
/// @return MATCH on match, NO_MATCH otherwise
//
int checkBreakpoint(Turing *turing, int curr_rule, int first_run)
{
  int match = FALSE;
  int start = TRUE;
  int return_value = FALSE;
  int state = turing->state_;
  int pos = turing->pos_;
  char read = turing->band_[pos];
  char write = turing->rules_[curr_rule]->sign_out_;

  if(first_run)
    return return_value;

  Breakpoint *prev_bp, *curr_bp = turing->breakpoint_;
  while(curr_bp)
  {
    if((curr_bp->mode_ == STATE && state == curr_bp->con_.number) ||
      (curr_bp->mode_ == POS && pos == curr_bp->con_.number && 
        curr_bp->band_cnt_ <= turing->band_cnt_))
    {
      match = TRUE;
      return_value = TRUE;
    }
    else if((curr_bp->mode_ == READ && read == curr_bp->con_.sign) || 
      (curr_bp->mode_ == WRITE && write == curr_bp->con_.sign))
    {
      match = TRUE;
      return_value = TRUE;
    }
    if(start)
    {
      if(match)
      {
        turing->breakpoint_ = curr_bp->next_;
        free(curr_bp);
        turing->breakpoint_length_--;
        curr_bp = turing->breakpoint_;
      }
      else
      {
        start = FALSE;
        prev_bp = curr_bp;
        curr_bp = curr_bp->next_;
      }
    }
    else
    {
      if(match)
      {
        prev_bp->next_ = curr_bp->next_;
        free(curr_bp);
        turing->breakpoint_length_--;
        curr_bp = prev_bp->next_;
      }
      else
      {
        prev_bp = curr_bp;
        curr_bp = curr_bp->next_;
      }
    }
    match = FALSE;
  }
  return return_value;
}


//-----------------------------------------------------------------------------
///
/// reallocBand
///
/// @param turing   Machine which is executed
/// @param parse    declares if band gets realloced while parsing
///
/// @return 0 on SUCESS, ERR_MEMORY if not enough memory is available
//
int reallocBand(Turing *turing, int parse)
{
  int init;
  char *tmp;
  char *save = malloc(BAND_WIDTH * turing->band_cnt_ * sizeof(char));
  memcpy(save, turing->band_, turing->band_cnt_ * BAND_WIDTH);
  tmp = realloc(turing->band_, BAND_WIDTH * ++turing->band_cnt_ * sizeof(char));

  if(tmp == NULL)
  {
    free(save);
    return ERR_MEMORY;
  }

  turing->band_ = tmp;

  for(init = 0; init < BAND_WIDTH * turing->band_cnt_; init++)
    turing->band_[init] = '_';

  if(!parse)
    turing->pos_ = (BAND_WIDTH / 2) + turing->pos_;

  Breakpoint *curr_bp = turing->breakpoint_;
  while(curr_bp)
  {
    if(curr_bp->mode_ == POS && curr_bp->band_cnt_ <= (turing->band_cnt_ - 1))
      curr_bp->con_.number = (BAND_WIDTH / 2) + curr_bp->con_.number;

    curr_bp = curr_bp->next_;
  }
  memcpy(turing->band_ + (BAND_WIDTH / 2), save, 
    BAND_WIDTH * (turing->band_cnt_ - 1));
  free(save);
  return SUCCESS;
}

//-----------------------------------------------------------------------------
///
/// getPosition
///
/// @param turing   Machine which is executed
/// @param pos input position whitch needs to be converted
///
/// @return converted position
//
int getPosition(Turing *turing, signed int pos, int band_cnt)
{
  return ((BAND_WIDTH * band_cnt) / 2) + pos;
}

//-----------------------------------------------------------------------------
///
/// printRule
///
/// @param turing   Machine which is executed
/// @param rule_num The position in rules array
//
void printRule(Turing *turing, int rule_num)
{
  printf("%d %c -> %c %d %c\n", turing->rules_[rule_num]->state_in_,
    turing->rules_[rule_num]->sign_in_,
    turing->rules_[rule_num]->sign_out_,
    turing->rules_[rule_num]->state_out_,
    turing->rules_[rule_num]->direction_);
}

//-----------------------------------------------------------------------------
///
/// errorHandler
///
/// @param turing         Machine which is executed
/// @param returned_value declares error typ
/// @param freeMemory     free Memory yes/no?
///
/// @return returned_value (Error type)
//
int errorHandler(Turing *turing, int returned_value, int freeMemory)
{
  switch(returned_value)
  {
    case SUCCESS:
      return SUCCESS;
    case ERR_USAGE:
      printf("[ERR] usage: ./assb <file>\n");
      break;
    case ERR_MEMORY:
      printf("[ERR] out of memory\n");
      break;
    case ERR_INPUT:
      printf("[ERR] parsing of input failed\n");
      break;
    case ERR_FILE:
      printf("[ERR] reading the file failed\n");
      break;
    case ERR_NODMTM:
      printf("[ERR] non-deterministic turing machine\n");
      break;
    default:
      return SUCCESS;
  }
  if((returned_value != SUCCESS) && (returned_value != ERR_USAGE) && 
    freeMemory)
    freeMem(turing);
  return returned_value;
}

//-----------------------------------------------------------------------------
///
/// getLine
/// http://stackoverflow.com/questions/4023895/how-to-read-string-entered-by-
/// user-in-c
///
/// @param prmpt    prompt string
/// @param buff     buffer for input
/// @param sz       size of buffer
//
static int getLine (char *prmpt, char *buff, size_t sz) 
{
    int ch, extra;

    if (prmpt != NULL) 
    {
        printf("%s", prmpt);
        fflush(stdout);
    }
    if (fgets(buff, sz, stdin) == NULL)
    {
      return QUIT;
    }
    
    if (buff[strlen(buff) - 1] != '\n') 
    {
        extra = 0;
        while (((ch = getchar()) != '\n') && (ch != EOF))
            extra = 1;
        return (extra == 1) ? TOO_LONG : OK;
    }

    buff[strlen(buff) - 1] = '\0';
    return OK;
}
