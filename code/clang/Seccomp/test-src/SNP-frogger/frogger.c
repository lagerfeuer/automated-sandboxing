#include "curses.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>

//Config params for gameplay
#define SCREEN_WIDTH  80     // width of game window
#define SCREEN_HEIGHT 23     // height of game window
#define GAME_SPEED    75000  // redraw speed of game
#define NUM_LANES     15     // number of different lanes on motorway
#define OUTER_BORDER  2      // border around game window
#define GRASS_BORDER  2      // border on top and bottom of game window

//Display characters for froggy and cars
#define FROGGY       '@'
#define CAR          'X'

//Constants to represent user input
#define LEFT         1   // 'a'
#define RIGHT        2   // 'd'
#define UP           3   // 'w'
#define DOWN         4   // 's'
#define QUIT         5   // 'q'

// Represents a specific screen positions by Cartesian coordinates
typedef struct{
    int x;
    int y;
} Point;

// Cars on the motorway are organized using a linked list
// Every list element stores the car's coordinates and a pointer to the next car
typedef struct Car {
    Point position;
    struct Car* next;
} Car;

// List of cars on motorway (pointer to first list element)
Car* motorway;

// Position of froggy on screen
Point froggy;

// Variables to represent current game state
unsigned int error;
unsigned int quit;
unsigned int player_lost;
unsigned int player_won;

// Other global variables
WINDOW* screen;
int old_cursor = 0;

// TODO: add new global variables - you might need them to handle user input.
char user_input = 'x';

// TODO: add new function - you might need one to handle user input.
void* input_thread(void* arg) 
{
    while((user_input != 'q') && !quit && !error && !player_lost && !player_won)
        user_input = getchar();
    pthread_exit(0);
}

/**
 * inserts a new car to the motorway on a random lane (insertion is done at the beginning of list)
 * @param start_position x coordinate of where car should start
 */
void startCar(int start_position)
{
    Car* new_car = (Car*)malloc(sizeof(Car));
    if(!new_car)
    {
        error = 1;
        return;
    }

    int lane = ((unsigned int)rand()) % NUM_LANES;
    new_car->position.x = start_position;
    new_car->position.y = SCREEN_HEIGHT - (GRASS_BORDER + OUTER_BORDER + lane);
    new_car->next = motorway;
    motorway = new_car;
}

/**
 * creates initial set of cars which form motorway at the beginning
 */
void initMotorway()
{
    motorway = (Car*)malloc(sizeof(Car));
    if(!motorway)
    {
        error = 1;
        return;
    }
    int lane = ((unsigned int)rand()) % NUM_LANES;
    motorway->position.x = SCREEN_WIDTH / 3;
    motorway->position.y = SCREEN_HEIGHT - (4+lane);
    motorway->next = 0;

    int i;
    for(i = SCREEN_WIDTH / 3 + 1; i < SCREEN_WIDTH - OUTER_BORDER; i++)
        startCar(i);
}

/**
 * makes all components (froggy and all cars) visible on screen
 */
void drawScreen()
{
    werase(screen);
    box(screen, ' ', '#');

    //Draw froggy
    move(froggy.y, froggy.x);
    addch(FROGGY);

    //Draw all cars
    Car* active_car = motorway;
    while(active_car) {
        move(active_car->position.y, active_car->position.x);
        addch(CAR);
        active_car = active_car->next;
    }

    refresh();
}

//TODO: add handling of user input in this function
/**
 * calculates new position of froggy depending on what key the user pressed
 * this also includes check if froggy is on the other road or user pressed quit
 */
void moveFroggy()
{
    //    froggy.y = SCREEN_HEIGHT - OUTER_BORDER;


    if((user_input == 'a') && (froggy.x > OUTER_BORDER))
        froggy.x--;
    if((user_input == 'd') && (froggy.x < (SCREEN_WIDTH - OUTER_BORDER)))
        froggy.x++;
    if ((user_input == 'w') && (froggy.y >= (SCREEN_HEIGHT - NUM_LANES - OUTER_BORDER - GRASS_BORDER)))
        froggy.y--;
    if ((user_input == 's') && (froggy.y < (SCREEN_HEIGHT - OUTER_BORDER)))
        froggy.y++;

    if(user_input == 'q')
        quit = 1;

    if(froggy.y <= (SCREEN_HEIGHT - NUM_LANES - OUTER_BORDER - GRASS_BORDER))
        player_won = 1;
}

/**
 * iterates through list of cars
 * if car is about to leave screen - delete it
 * else: simply decrease value of x coordinate
 * also includes a collission check of froggy and any car
 */
void moveCarsOnMotorway()
{
    Car* active_car = motorway;
    Car* previous = 0;
    while(active_car)
    {
        //Do a collision check of car and froggy
        if(froggy.x == active_car->position.x &&
           froggy.y == active_car->position.y)
            player_lost = 1;

        active_car->position.x--;

        if(active_car->position.x == OUTER_BORDER)
        {
            //Car leaves screen
            Car* next = active_car->next;
            if(previous)
                previous->next = next;
            else
                motorway = next;

            free(active_car);
            active_car = next;
        }
        else
        {
            //Car continues moving
            previous = active_car;
            active_car = active_car->next;
        }
    }
}

/**
 * stops all cars which are currently driving on motorway
 */
void clearMotorway()
{
    Car* stop_car = motorway;
    while(stop_car)
    {
        Car* next = stop_car->next;
        free(stop_car);
        stop_car = next;
    }
}

/**
 * draw all components on screen to be shown when game ended
 */
void drawEndOfGameScreen()
{
    werase(screen);
    box(screen, 0, 0);
    move(SCREEN_HEIGHT/2-1, 10);
    if(player_won)
        printw("Congratulations. You won! Froggy crossed the motorway safely.");
    else
        printw("Game over. You lost! Froggy died.");
    move(SCREEN_HEIGHT/2, 10);
    printf("Press any key to exit.");
    refresh();
    getchar();
}

//TODO: add initialization and cleanup steps concerning user input
/**
 * performs initialization of game components
 * starts gameplay loop
 * does some cleanup steps
 * @param argc <<unused>>
 * @param argv <<unused>>
 * @return
 */
int main(int argc, char** argv)
{
    //Initialize game components

    srand(time(0));

    screen = initscr();
    if(screen == NULL)
        exit(-1);
    wresize(screen, SCREEN_HEIGHT, SCREEN_WIDTH );
    old_cursor = curs_set(0);

    froggy.x = SCREEN_WIDTH / 2;
    froggy.y = SCREEN_HEIGHT - OUTER_BORDER - 1;

    initMotorway();

    //Start game
    //TODO: add appropiate steps to start game here

    pthread_t tid;
    pthread_create(&tid, NULL, input_thread, NULL);

    while(!quit && !error && !player_lost && !player_won)
    {
        moveFroggy();
        user_input = 'x';
        moveCarsOnMotorway();
        startCar((SCREEN_WIDTH - OUTER_BORDER));
        drawScreen();
        usleep(GAME_SPEED);
    }

    //Clean up game
    //TODO: don't forget proper cleanup
    clearMotorway();

    if(player_lost || player_won)
        drawEndOfGameScreen();
    
    pthread_cancel(tid);
    pthread_join(tid, NULL);
    delwin(screen);
    curs_set(old_cursor);
    endwin();
    refresh();

    return error;
}
