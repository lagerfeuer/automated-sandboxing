#ifndef TEST_H
#define TEST_H

#include <stdlib.h>

#define SIZE 100

typedef struct TEST_STRUCT
{
  unsigned char* data;
  size_t len;
  size_t size;
} test_struct;

ssize_t init(test_struct* t);
void read_in(test_struct* t);
void print(test_struct* t);
void destroy(test_struct* t);
#endif
