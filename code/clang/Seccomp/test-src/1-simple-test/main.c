#include "test.h"
#include <unistd.h>
#include <string.h>

int main(int argc, const char* argv[]) {
  test_struct t;
  if(init(&t))
    return 1;
  
  char* msg = "Hello, enter your stuff:\n";
  write(STDOUT_FILENO, msg, strlen(msg));
  read_in(&t);
  print(&t);
  destroy(&t);

  return 0;
}
