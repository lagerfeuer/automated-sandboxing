#include "test.h"
#include <unistd.h>

ssize_t init(test_struct* t) {
  t->len = 0;
  t->size = SIZE;
  t->data = malloc(SIZE);
  if(t->data == NULL)
    return -1;
  return 0;
}

void read_in(test_struct* t) {
  t->len = read(STDIN_FILENO, t->data, t->size);
}

void print(test_struct* t) {
  write(STDOUT_FILENO, t->data, t->len);
}

void destroy(test_struct* t) {
  t->len = 0;
  t->size = 0;
  free(t->data);
}
