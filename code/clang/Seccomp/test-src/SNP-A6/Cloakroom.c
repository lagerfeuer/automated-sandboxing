#include <stdio.h>
#include "Cloakroom.h"
#include "Basin.h"
#include "Soap.h"
#include "Toilet.h"
#include "Attendant.h"

void Cloakroom_init(struct Cloakroom* this, unsigned int num_toilets,
                    unsigned int num_basins, unsigned int num_soaps)
{
  Thread_global_init();
  unsigned int i;
  vector_init(&this->free_toilets_);
  for (i = 1; i <= num_toilets; ++i)
    vector_push_back(&this->free_toilets_, new_Toilet(this, i));
  vector_init(&this->free_basins_);
  for (i = 1; i <= num_basins; ++i)
    vector_push_back(&this->free_basins_, new_Basin(this, i));
  vector_init(&this->free_soaps_);
  for (i = 1; i <= num_soaps; ++i)
    vector_push_back(&this->free_soaps_, new_Soap(this, i));

  pthread_mutex_init(&this->toilet_lock_, NULL);
  pthread_mutex_init(&this->basin_lock_, NULL);
  pthread_mutex_init(&this->soap_lock_, NULL);

  printf("Cloakroom is ready.\n");
}

struct Toilet* Cloakroom_enterToilet(struct Cloakroom* this, struct Thread* thread, int for_cleaning)
{
  struct Toilet* result = 0;
  pthread_mutex_lock(&this->toilet_lock_);
  vector_iterator it = vector_begin(&this->free_toilets_);
  for (; it != vector_end(&this->free_toilets_); ++it)
  {
    Toilet_lockDoor(*it, thread);
    if (Toilet_isDirty(*it, thread) == for_cleaning)
    {
      result = *it;
      vector_erase(&this->free_toilets_, it);
      break;
    }
    Toilet_unlockDoor(*it, thread);
  }
  pthread_mutex_unlock(&this->toilet_lock_);
  return result;
}

struct Basin* Cloakroom_acquireBasin(struct Cloakroom* this, struct Thread* thread, int for_cleaning)
{
  struct Basin* result = 0;
  pthread_mutex_lock(&this->basin_lock_);
  vector_iterator it = vector_begin(&this->free_basins_);
  for (; it != vector_end(&this->free_basins_); ++it)
  {
    Basin_acquireBasin(*it, thread);
    if (Basin_isDirty(*it, thread) == for_cleaning)
    {
      result = *it;
      vector_erase(&this->free_basins_, it);
      break;
    }
    Basin_releaseBasin(*it, thread);
  }
  pthread_mutex_unlock(&this->basin_lock_);
  return result;
}

struct Soap* Cloakroom_acquireSoap(struct Cloakroom* this, struct Thread* thread, int for_cleaning)
{
  struct Soap* result = 0;
  pthread_mutex_lock(&this->soap_lock_);
  vector_iterator it = vector_begin(&this->free_soaps_);
  for (; it != vector_end(&this->free_soaps_); ++it)
  {
    Soap_takeSoap(*it, thread);
    if (Soap_isEmpty(*it, thread) == for_cleaning)
    {
      result = *it;
      vector_erase(&this->free_soaps_, it);
      break;
    }
    Soap_returnSoap(*it, thread);
  }
  pthread_mutex_unlock(&this->soap_lock_);
  return result;
}

void Cloakroom_leaveToilet(struct Cloakroom* this, struct Thread* thread, struct Toilet* toilet)
{
  pthread_mutex_lock(&this->toilet_lock_);
  Toilet_unlockDoor(toilet, thread);
  vector_push_back(&this->free_toilets_, toilet);
  pthread_mutex_unlock(&this->toilet_lock_);
}

void Cloakroom_leaveBasin(struct Cloakroom* this, struct Thread* thread, struct Basin* basin)
{
  pthread_mutex_lock(&this->basin_lock_);
  Basin_releaseBasin(basin, thread);
  vector_push_back(&this->free_basins_, basin);
  pthread_mutex_unlock(&this->basin_lock_);
}

void Cloakroom_returnSoap(struct Cloakroom* this, struct Thread* thread, struct Soap* soap)
{
  pthread_mutex_lock(&this->soap_lock_);
  Soap_returnSoap(soap, thread);
  vector_push_back(&this->free_soaps_, soap);
  pthread_mutex_unlock(&this->soap_lock_);
}

void Cloakroom_destroy(struct Cloakroom* this) {
  vector_iterator it = vector_begin(&this->free_toilets_);
  while(it != vector_end(&this->free_toilets_)) {
    destroy_Toilet(*it);
    vector_erase(&this->free_toilets_, it);
  }
  vector_destroy(&this->free_toilets_);

  it = vector_begin(&this->free_soaps_);
  while(it != vector_end(&this->free_soaps_)) {
    destroy_Soap(*it);
    vector_erase(&this->free_soaps_, it);
  }
  vector_destroy(&this->free_soaps_);

  it = vector_begin(&this->free_basins_);
  while(it != vector_end(&this->free_basins_)) {
    destroy_Basin(*it);
    vector_erase(&this->free_basins_, it);
  }
  vector_destroy(&this->free_basins_);

  pthread_mutex_destroy(&this->toilet_lock_);
  pthread_mutex_destroy(&this->basin_lock_);
  pthread_mutex_destroy(&this->soap_lock_);
}
