#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "Attendant.h"
#include "Customer.h"
#include "Basin.h"
#include "Soap.h"
#include "common.h"

struct Basin* new_Basin(struct Cloakroom* cloakroom, unsigned int counter)
{
  struct Basin* this = malloc(sizeof(struct Basin));
  this->cloakroom_ = cloakroom;
  this->number_ = counter;
  this->usage_counter_ = 0;
  this->max_usage_counter_ = 10;
  this->locked_by_ = 0;
  pthread_mutex_init(&this->lock_, NULL);

  return this;
}

void destroy_Basin(struct Basin* this)
{
  pthread_mutex_destroy(&this->lock_);
  free(this);
}

int Basin_isDirty(struct Basin* this, struct Thread* thread)
{
  assert(this->locked_by_ == thread);
  return (this->usage_counter_ >= this->max_usage_counter_);
}

void Basin_washHands(struct Basin* this, struct Customer* customer, struct Soap* soap)
{
  assert(this->locked_by_ == customer);
  assert(this->usage_counter_ < this->max_usage_counter_);
  Soap_use(soap, customer);
  printf("%s is washing his hands in basin %u using soap %u\n", customer->name_, this->number_, soap->number_);
  sleep_milliseconds(100);
  ++(this->usage_counter_);
}

void Basin_clean(struct Basin* this, struct Attendant* attendant)
{
  assert(this->locked_by_ == attendant);
  assert(this->usage_counter_ == this->max_usage_counter_);
  printf("%s cleaned basin %u.\n", attendant->name_, this->number_);
  sleep_milliseconds(1000);
  this->usage_counter_ = 0;
}

void Basin_acquireBasin(struct Basin* this, struct Thread* thread)
{
  pthread_mutex_lock(&this->lock_);
  assert(this->locked_by_ == 0);
  this->locked_by_ = thread;
}

void Basin_releaseBasin(struct Basin* this, struct Thread* thread)
{
  assert(this->locked_by_ == thread);
  this->locked_by_ = 0;
  pthread_mutex_unlock(&this->lock_);
}
