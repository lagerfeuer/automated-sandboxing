#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "vector.h"

void expand(struct vector* this)
{
  if (this->used_ + 1 == this->size_)
  {
    this->size_ *= 2;
    void* temp = realloc(this->data_, this->size_ * sizeof(void*));
    if (temp)
    {
      this->data_ = temp;
    }
    else
    {
      printf("out of memory\n");
      exit(-1);
    }
  }
}

vector_iterator vector_begin(struct vector* this)
{
  return this->data_;
}

vector_iterator vector_end(struct vector* this)
{
  return this->data_ + this->used_;
}

void vector_erase(struct vector* this, vector_iterator it)
{
  vector_iterator next = it + 1;
  for (; next < vector_end(this); ++it, ++next)
    *it = *next;
  --this->used_;
}

void vector_push_back(struct vector* this, void* element)
{
  expand(this);
  this->data_[this->used_++] = element;
}

void vector_init(struct vector* this)
{
  this->size_ = 16;
  this->used_ = 0;
  this->data_ = malloc(16 * sizeof(void*));
}

void vector_destroy(struct vector* this) {
  free(this->data_);
}
