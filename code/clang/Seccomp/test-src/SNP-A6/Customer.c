#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "Customer.h"
#include "Cloakroom.h"
#include "Toilet.h"
#include "Basin.h"

atomic_int g_num_customers = ATOMIC_VAR_INIT(0);

void* Customer_run(struct Customer* this);

struct Customer* new_Customer(struct Cloakroom* cloakroom, unsigned int counter)
{
  atomic_fetch_add(&g_num_customers, 1);

  struct Customer* this = malloc(sizeof(struct Customer));
  this->cloakroom_ = cloakroom;
  sprintf(this->name_, "Customer %u", counter);
  Thread_start((void*(*)(void*))Customer_run, this);
  return this;
}

void* Customer_run(struct Customer* this)
{
  printf("%s arrived.\n", this->name_);
  struct Toilet* toilet = Cloakroom_enterToilet(this->cloakroom_, (struct Thread*) this, 0);
  if (toilet != 0)
  {
    Toilet_use(toilet, this);
    Cloakroom_leaveToilet(this->cloakroom_, (struct Thread*) this, toilet);

    /*-------------------------------------------------------*/
    //TODO: you still have to implement:
    // acquire basin and soap
    struct Basin* basin = Cloakroom_acquireBasin(this->cloakroom_, (struct Thread*) this, 0);
    while(basin == 0)
      basin = Cloakroom_acquireBasin(this->cloakroom_, (struct Thread*) this, 0);
    struct Soap* soap = Cloakroom_acquireSoap(this->cloakroom_, (struct Thread*) this, 0);
    while(soap == 0)
      soap = Cloakroom_acquireSoap(this->cloakroom_, (struct Thread*) this, 0);

    /*-------------------------------------------------------*/
    Basin_washHands(basin, this, soap);

    Cloakroom_returnSoap(this->cloakroom_, (struct Thread*) this, soap);
    Cloakroom_leaveBasin(this->cloakroom_, (struct Thread*) this, basin);
  }
  else
  {
    printf("%s is swearing and shouting because all free toilets are dirty.\n", this->name_);
  }
  printf("%s left.\n", this->name_);
  free(this);

  atomic_fetch_sub(&g_num_customers, 1);
  Thread_exit(0);
  return 0;
}
