#ifndef TOILET_H
#define TOILET_H

#include <semaphore.h>

struct Cloakroom;
struct Customer;
struct Thread;
struct Attendant;

struct Toilet
{
  struct Cloakroom* cloakroom_;
  unsigned int number_;

  unsigned int usage_counter_;
  unsigned int max_usage_counter_;

  void* locked_by_;
  pthread_mutex_t lock_;
};

struct Toilet* new_Toilet(struct Cloakroom* cloakroom, unsigned int counter);
void destroy_Toilet(struct Toilet* this);
void Toilet_use(struct Toilet* this, struct Customer* customer);
int Toilet_isDirty(struct Toilet* this, struct Thread* thread);
void Toilet_clean(struct Toilet* this, struct Attendant* attendant);
void Toilet_lockDoor(struct Toilet* this, struct Thread* thread);
void Toilet_unlockDoor(struct Toilet* this, struct Thread* thread);


#endif // TOILET_H
