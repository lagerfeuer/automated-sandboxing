#pragma once

#include <stdbool.h>
#include <stdint.h>

/// Checks if the input is between lower(inclusive) and upper (inclusive)
bool in_range(int input, int low, int high);

/// Sleeps for n milliseconds.
/// Actual sleep duration is not guaranteed; may be lower due to signal
/// handlers being executed.
void sleep_milliseconds(int milliseconds);

/// Gets a random 32 bit integer from a reasonable source of randomness
/// (/dev/urandom on linux).

uint32_t better_rand();

uint32_t generate_pen_id();

uint32_t generate_list_id();
