#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Toilet.h"
#include "Customer.h"
#include "Attendant.h"
#include "common.h"

struct Toilet* new_Toilet(struct Cloakroom* cloakroom, unsigned int counter)
{
  struct Toilet* this = malloc(sizeof(struct Toilet));
  printf("Created Toilet %u\n", counter);
  this->cloakroom_ = cloakroom;
  this->number_ = counter;
  this->usage_counter_ = 0;
  this->max_usage_counter_ = 5;
  this->locked_by_ = 0;
  pthread_mutex_init(&this->lock_, NULL);

  return this;
}

void destroy_Toilet(struct Toilet* this)
{
  pthread_mutex_destroy(&this->lock_);
  free(this);
}

void Toilet_use(struct Toilet* this, struct Customer* customer)
{
  assert(this->locked_by_ == customer);
  printf("%s uses toilet %u.\n", customer->name_, this->number_);
  sleep_milliseconds(100);
  ++this->usage_counter_;
}

int Toilet_isDirty(struct Toilet* this, struct Thread* thread)
{
  assert(this->locked_by_ == thread);
  if (this->usage_counter_ >= this->max_usage_counter_)
    return 1;
  return 0;
}

void Toilet_clean(struct Toilet* this, struct Attendant* attendant)
{
  assert(this->locked_by_ == attendant);
  assert(this->usage_counter_ == this->max_usage_counter_);
  printf("%s cleaned toilet %u.\n", attendant->name_, this->number_);
  sleep_milliseconds(1000);
  this->usage_counter_ = 0;
}

void Toilet_lockDoor(struct Toilet* this, struct Thread* thread)
{
  pthread_mutex_lock(&this->lock_);
  assert(this->locked_by_ == 0);
  this->locked_by_ = thread;
}

void Toilet_unlockDoor(struct Toilet* this, struct Thread* thread)
{
  assert(this->locked_by_ == thread);
  this->locked_by_ = 0;
  pthread_mutex_unlock(&this->lock_);
}
