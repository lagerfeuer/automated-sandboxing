#ifndef BASIN_H
#define BASIN_H

#include <semaphore.h>

struct Cloakroom;
struct Thread;
struct Customer;
struct Soap;
struct Attendant;

struct Basin
{
  struct Cloakroom* cloakroom_;
  unsigned int number_;

  unsigned int usage_counter_;
  unsigned int max_usage_counter_;

  void* locked_by_;
  pthread_mutex_t lock_;
};

struct Basin* new_Basin(struct Cloakroom* cloakroom, unsigned int counter);
void destroy_Basin(struct Basin* this);
void Basin_washHands(struct Basin* this, struct Customer* customer, struct Soap* soap);
int Basin_isDirty(struct Basin* this, struct Thread* thread);
void Basin_clean(struct Basin* this, struct Attendant* attendant);
void Basin_acquireBasin(struct Basin* this, struct Thread* thread);
void Basin_releaseBasin(struct Basin* this, struct Thread* thread);

#endif // BASIN_H
