#ifndef CUSTOMER_H
#define CUSTOMER_H

#include "Thread.h"
#include <stdatomic.h>

struct Cloakroom;

extern atomic_int g_num_customers;

struct Customer
{
  THREAD_BASE;
  struct Cloakroom* cloakroom_;
  char name_[20];
};

struct Customer* new_Customer(struct Cloakroom* cloakroom, unsigned int counter);

#endif // CUSTOMER_H
