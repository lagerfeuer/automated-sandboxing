#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdatomic.h>

#include "Attendant.h"
#include "Toilet.h"
#include "Basin.h"
#include "Soap.h"
#include "Thread.h"
#include "Cloakroom.h"
#include "Customer.h"
#include "common.h"


const char* Attendant_NAMES[] = {"Christian", "Daniel", "Jakob",
                                  "Manuel",    "Markus", "Peter"};

void* Attendant_run(struct Attendant* this);

struct Attendant* new_Attendant(struct Cloakroom* cloakroom, unsigned int counter)
{
  struct Attendant* this = malloc(sizeof(struct Attendant));
  this->cloakroom_ = cloakroom;
  this->name_ = Attendant_NAMES[counter % MAX_ATTENDANT_COUNT];
  printf("Attendant %s arrived.\n", this->name_);
  Thread_start((void*(*)(void*))Attendant_run, this);
  return this;
}

void* Attendant_run(struct Attendant* this)
{
  while (!atomic_load(&g_exit_requested) || atomic_load(&g_num_customers) > 0)
  {
    struct Toilet* toilet = Cloakroom_enterToilet(this->cloakroom_, (struct Thread*) this, 1);
    if (toilet)
    {
      Toilet_clean(toilet, this);
      Cloakroom_leaveToilet(this->cloakroom_, (struct Thread*) this, toilet);
      sleep_milliseconds(100);
    }
    struct Basin* basin = Cloakroom_acquireBasin(this->cloakroom_, (struct Thread*) this, 1);
    if (basin)
    {
      Basin_clean(basin, this);
      Cloakroom_leaveBasin(this->cloakroom_, (struct Thread*) this, basin);
      sleep_milliseconds(100);
    }
    struct Soap* soap = Cloakroom_acquireSoap(this->cloakroom_, (struct Thread*) this, 1);
    if (soap)
    {
      Soap_refill(soap, this);
      Cloakroom_returnSoap(this->cloakroom_, (struct Thread*) this, soap);
      sleep_milliseconds(100);
    }
    sleep_milliseconds(100);
  }
  printf("Attendant %s left.\n", this->name_);
  free(this);
  Thread_exit(0);
  return 0;
}
