#ifndef CLOAKROOM_H
#define CLOAKROOM_H

#include <pthread.h>
#include "vector.h"

void backtrace_assert(int exp);

struct Thread;

struct Cloakroom
{
  struct vector free_toilets_;
  struct vector free_basins_;
  struct vector free_soaps_;

  pthread_mutex_t toilet_lock_;
  pthread_mutex_t basin_lock_;
  pthread_mutex_t soap_lock_;

};

void Cloakroom_init(struct Cloakroom* this, unsigned int num_toilets,
                    unsigned int num_basins, unsigned int num_soaps);

void Cloakroom_destroy(struct Cloakroom* this);

struct Toilet* Cloakroom_enterToilet(struct Cloakroom* this, struct Thread* thread, int for_cleaning);
struct Basin* Cloakroom_acquireBasin(struct Cloakroom* this, struct Thread* thread, int for_cleaning);
struct Soap* Cloakroom_acquireSoap(struct Cloakroom* this, struct Thread* thread, int for_cleaning);

void Cloakroom_leaveToilet(struct Cloakroom* this, struct Thread* thread, struct Toilet* toilet);
void Cloakroom_leaveBasin(struct Cloakroom* this, struct Thread* thread, struct Basin* basin);
void Cloakroom_returnSoap(struct Cloakroom* this, struct Thread* thread, struct Soap* soap);

#endif // CLOAKROOM_H
