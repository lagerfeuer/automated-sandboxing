#ifndef VECTOR_H
#define VECTOR_H

struct vector // please note that this is a void* vector,
              // not a general purpose vector
{
  void** data_;
  unsigned int size_;
  unsigned int used_;
};

typedef void** vector_iterator;

void new_vector(struct vector* this);
void vector_push_back(struct vector* this, void* element);
void vector_init(struct vector* this);
void vector_destroy(struct vector* this);
vector_iterator vector_begin(struct vector* this);
vector_iterator vector_end(struct vector* this);
void vector_erase(struct vector* this, vector_iterator it);

#endif // VECTOR_H
