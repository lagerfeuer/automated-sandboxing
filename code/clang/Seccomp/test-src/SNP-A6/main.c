#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <assert.h>

#include "vector.h"
#include "common.h"
#include "Toilet.h"
#include "Basin.h"
#include "Soap.h"
#include "Customer.h"
#include "Attendant.h"
#include "Cloakroom.h"


///////////////////////////////////////////////////////////////////////////////

void check_arg(const char* name, int input, int low, int high)
{
  if (!in_range(input, low, high))
  {
    printf("Error: %s must be in range [%d, %d], given: %d\n", name, low, high, input);
    exit(-1);
  }
}


///////////////////////////////////////////////////////////////////////////////


int main(int argc, char* argv[])
{
  if (argc < 7)
  {
    printf("Usage: cloakroom <num_toilets> <num_basins> <num_soaps> "
           "<num_attendants> <num_customers_per_sec> <seconds_to_run (0 means forever)>\n");
    return -1;
  }

  unsigned int num_toilets    = strtoul(argv[1], NULL, 0);
  unsigned int num_basins     = strtoul(argv[2], NULL, 0);
  unsigned int num_soaps      = strtoul(argv[3], NULL, 0);
  unsigned int num_attendants = strtoul(argv[4], NULL, 0);
  unsigned int customer_rate  = strtoul(argv[5], NULL, 0);
  unsigned int seconds_to_run = strtoul(argv[6], NULL, 0);

  check_arg("num_toilets", num_toilets, 1, 100);
  check_arg("num_basins", num_basins, 1, 100);
  check_arg("num_soaps", num_soaps, 1, 100);
  check_arg("customer_rate", customer_rate, 1, 100);
  check_arg("num_attendants", num_attendants, 1, MAX_ATTENDANT_COUNT);
  check_arg("seconds_to_run", seconds_to_run, 0, 3600);

  printf("A cloakroom with %u toilets, %u basins, %u soaps,"
         "%u customers per second, %u attendants.\n",
         num_toilets, num_basins, num_soaps, customer_rate, num_attendants);

  struct Cloakroom cloakroom;
  Cloakroom_init(&cloakroom, num_toilets, num_basins, num_soaps);

  unsigned int i;
  for (i = 1; i <= num_attendants; i++)
    new_Attendant(&cloakroom, i);

  unsigned int time_limit = seconds_to_run * 1000;
  unsigned int milliseconds_passed = 0; 

  i = 1;
  while (seconds_to_run == 0 || milliseconds_passed < time_limit)
  {
    new_Customer(&cloakroom, i++);
    sleep_milliseconds(1000 / customer_rate);
    milliseconds_passed += 1000 / customer_rate;
  }

  Thread_request_exit_and_wait_for_all();
  Cloakroom_destroy(&cloakroom);
  puts("Program terminated successfully!");
  return 0;
}
