#pragma once

#include <pthread.h>
#include <stdbool.h>
#include <stdatomic.h>

///////////////////////////////////////////////////////////////////////////////

extern pthread_mutex_t g_thread_count_mutex;
extern pthread_cond_t g_thread_exit_cond;
extern int g_thread_count;
extern atomic_int g_exit_requested;

///////////////////////////////////////////////////////////////////////////////

#define THREAD_BASE pthread_t tid_

struct Thread
{
  THREAD_BASE;
};

///////////////////////////////////////////////////////////////////////////////

void Thread_start(void* (*function)(void*), void* this);

///////////////////////////////////////////////////////////////////////////////

///
/// Must be called by the started thread to terminate it.
/// Calls pthread_exit after updating the thread counter.
///
void Thread_exit(void* result);

///////////////////////////////////////////////////////////////////////////////

///
/// Waits for the termination of all threads started with Thread_start
///
void Thread_request_exit_and_wait_for_all();

///////////////////////////////////////////////////////////////////////////////

///
/// Initializes the global thread count mutex and condition.
///
void Thread_global_init();
