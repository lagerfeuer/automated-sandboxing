#ifndef ATTENDANT_H
#define ATTENDANT_H

#include "Thread.h"

#define MAX_ATTENDANT_COUNT 6

struct Attendant
{
  THREAD_BASE;
  struct Cloakroom* cloakroom_;
  const char* name_;
};

struct Attendant* new_Attendant(struct Cloakroom* cloakroom, unsigned int counter);

#endif // ATTENDANT_H
