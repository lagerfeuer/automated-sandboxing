#include "common.h"

#include "time.h"

#include <stdio.h>
#include <stdlib.h>

///////////////////////////////////////////////////////////////////////////////

bool in_range(int input, int low, int high)
{
  return input >= low && input <= high;
}

///////////////////////////////////////////////////////////////////////////////

void sleep_milliseconds(int milliseconds)
{
  time_t seconds = milliseconds / 1000;
  long nanoseconds = (milliseconds % 1000) * 1000L * 1000L;
  struct timespec sleep_time = {.tv_nsec = nanoseconds, .tv_sec = seconds};
  struct timespec remainder_time = {.tv_nsec = 0, .tv_sec = 0};
  nanosleep(&sleep_time, &remainder_time);
}

///////////////////////////////////////////////////////////////////////////////

uint32_t better_rand()
{
  FILE* random_device = fopen("/dev/urandom", "rb");
  uint32_t var = 0;
  size_t items_read = fread(&var, sizeof(uint32_t), 1, random_device);
  if (items_read != 1)
  {
    puts("Failed to read from random device");
    fclose(random_device);
    exit(1337);
  }
  fclose(random_device);
  return var;
}

///////////////////////////////////////////////////////////////////////////////

uint32_t generate_pen_id()
{
  return better_rand();
}

///////////////////////////////////////////////////////////////////////////////

uint32_t generate_list_id()
{
  return better_rand();
}

///////////////////////////////////////////////////////////////////////////////
