#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Soap.h"
#include "Attendant.h"
#include "Customer.h"
#include "common.h"

struct Soap* new_Soap(struct Cloakroom* cloakroom, unsigned int counter)
{
  struct Soap* this = malloc(sizeof(struct Soap));
  this->cloakroom_ = cloakroom;
  this->number_ = counter;
  this->usage_counter_ = 0;
  this->max_usage_counter_ = 20;
  this->locked_by_ = 0;
  pthread_mutex_init(&this->lock_, NULL);

  return this;
}

void destroy_Soap(struct Soap* this)
{
  pthread_mutex_destroy(&this->lock_);
  free(this);
}

void Soap_use(struct Soap* this, struct Customer* customer)
{
  assert(this->locked_by_ == customer);
  assert(this->usage_counter_ < this->max_usage_counter_);
  ++this->usage_counter_;
  sleep_milliseconds(100);
}

int Soap_isEmpty(struct Soap* this, struct Thread* thread)
{
  assert(this->locked_by_ == thread);
  return (this->usage_counter_ >= this->max_usage_counter_);
}

void Soap_refill(struct Soap* this, struct Attendant* attendant)
{
  assert(this->locked_by_ == attendant);
  assert(this->usage_counter_ == this->max_usage_counter_);
  printf("%s refilled soap %u.\n", attendant->name_, this->number_);
  this->usage_counter_ = 0;
  sleep_milliseconds(1000);
}

void Soap_takeSoap(struct Soap* this, struct Thread* thread)
{
  pthread_mutex_lock(&this->lock_);
  assert(this->locked_by_ == 0);
  this->locked_by_ = thread;
}

void Soap_returnSoap(struct Soap* this, struct Thread* thread)
{
  assert(this->locked_by_ == thread);
  this->locked_by_ = 0;
  pthread_mutex_unlock(&this->lock_);
}
