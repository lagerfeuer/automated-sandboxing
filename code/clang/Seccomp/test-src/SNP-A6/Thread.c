#include "Thread.h"

#include <stdlib.h>

///////////////////////////////////////////////////////////////////////////////

pthread_mutex_t g_thread_count_mutex;
pthread_cond_t g_thread_exit_cond;
int g_thread_count;
atomic_int g_exit_requested = ATOMIC_VAR_INIT(0);

///////////////////////////////////////////////////////////////////////////////

void Thread_exit(void* result)
{
  pthread_mutex_lock(&g_thread_count_mutex);
  g_thread_count--;
  if (g_thread_count <= 0)
  {
    pthread_cond_signal(&g_thread_exit_cond);
  }
  pthread_mutex_unlock(&g_thread_count_mutex);
  pthread_exit(result);
}

///////////////////////////////////////////////////////////////////////////////

void Thread_start(void* (*function)(void*), void* this)
{
  struct Thread* thread = (struct Thread*)this;
  //
  // Since we never join these threads,
  // detach is needed to avoid leaking OS resources
  //
  // We also cannot just call pthread_detach after the create
  // since the access into thread->tid_ for that would cause
  // a data race.
  //
  // The other possibility would be to use a local variable for
  // storing the thread id, and using pthread_self() to get the
  // thread id in the threads themselves.
  //

  pthread_mutex_lock(&g_thread_count_mutex);
  g_thread_count++;
  pthread_mutex_unlock(&g_thread_count_mutex);

  pthread_attr_t attributes;
  pthread_attr_init(&attributes);
  pthread_attr_setdetachstate(&attributes, PTHREAD_CREATE_DETACHED);
  pthread_create(&thread->tid_, &attributes, function, this);
}

///////////////////////////////////////////////////////////////////////////////

void Thread_global_init()
{
  pthread_mutex_init(&g_thread_count_mutex, 0);
  pthread_cond_init(&g_thread_exit_cond, 0);
  g_thread_count = 0;
  g_exit_requested = false;
}

///////////////////////////////////////////////////////////////////////////////

void Thread_request_exit_and_wait_for_all()
{
  g_exit_requested = true;
  pthread_mutex_lock(&g_thread_count_mutex);
  if (g_thread_count > 0)
  {
    pthread_cond_wait(&g_thread_exit_cond, &g_thread_count_mutex);
  }
  pthread_mutex_unlock(&g_thread_count_mutex);
}

///////////////////////////////////////////////////////////////////////////////
