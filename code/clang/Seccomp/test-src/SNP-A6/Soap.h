#ifndef SOAP_H
#define SOAP_H

#include <semaphore.h>

struct Cloakroom;
struct Customer;
struct Attendant;
struct Thread;

struct Soap
{
  struct Cloakroom* cloakroom_;
  unsigned int number_;

  unsigned int usage_counter_;
  unsigned int max_usage_counter_;

  void* locked_by_;
  pthread_mutex_t lock_;
};

struct Soap* new_Soap(struct Cloakroom* cloakroom, unsigned int counter);
void destroy_Soap(struct Soap* this);
void Soap_use(struct Soap* this, struct Customer* customer);
int Soap_isEmpty(struct Soap* this, struct Thread* thread);
void Soap_refill(struct Soap* this, struct Attendant* attendant);
void Soap_takeSoap(struct Soap* this, struct Thread* thread);
void Soap_returnSoap(struct Soap* this, struct Thread* thread);

#endif // SOAP_H
