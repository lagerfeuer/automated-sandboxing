#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NO_ASM 1

#ifdef NO_ASM
#include <unistd.h>
#endif

#include <readline/readline.h>

void hello_world() { printf("Hello World!\n"); }

void calculate() {
  char* line = readline("Number 1: ");
  int num1 = atoi(line);
  free(line);
  line = readline("Number 2: ");
  int num2 = atoi(line);
  free(line);
  printf("Result: %d\n", num1 + num2);
}

void execute() {
  char* args[2];
  args[0] = "fortune";
  args[1] = NULL;
  asm("call execvp" ::"D"(args[0]), "S"(args));
}

int main() {
  for (;;) {
    int selection = 0;
    printf("Make a selection:\n1) Hello World\n2) Add two numbers\n3) "
           "Execute!\n\n");
    char* line = readline("> ");
    if (strlen(line) == 0)
      return 0;
    selection = atoi(line);
    switch (selection) {
    case 1:
      hello_world();
      break;
    case 2:
      calculate();
      break;
    case 3:
      execute();
      break;
    default:
      printf("What?!\n");
    }
    printf("\n\n");
    free(line);
  }
  return 0;
}
