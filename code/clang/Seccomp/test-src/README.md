# Requirements

* diet-libc (`diet` executable in path) - libc implementation we analyze and link against
* Bear ([Build EAR](https://github.com/rizsotto/Bear)) - used to generate `compile_commands.json` from Makefiles
* auto-seccomp tool (installed somewhere in `$PATH`) - generate the actual seccomp filters (needs analyze output and `compile_commands.json`

# Building

## Normal Build (Standard LibC)

```sh
make seccomp # Optional
make
```

## Diet LibC Build

```sh
make seccomp # Optional
make LINK_DIET_LIBC=y
```
