#include <pthread.h>
#include <sched.h>
#include <stdio.h>

pthread_mutex_t m;

#define SIZE 5

void *func(void *args) {
  unsigned int num = *((unsigned int *)args);
  pthread_mutex_lock(&m);
  printf("Hello %u\n", num);
  pthread_mutex_unlock(&m);
  sched_yield();
  pthread_mutex_lock(&m);
  printf("Bye %u\n", num);
  pthread_mutex_unlock(&m);
  return NULL;
}

int main() {
  pthread_mutex_init(&m, NULL);
  pthread_t tids[SIZE];
  unsigned int args[SIZE];
  unsigned int i;
  for (i = 0; i < SIZE; i++)
    args[i] = i + 1;
  printf("Creating in main thread with TID %zd\n", pthread_self());
  for (i = 0; i < SIZE; i++)
    pthread_create(&tids[i], NULL, &func, (void *)&args[i]);
  sched_yield();
  printf("Joining\n");
  for (i = 0; i < SIZE; i++)
    pthread_join(tids[i], NULL);
  pthread_mutex_destroy(&m);
  printf("Finished\n");
}
