#include <stdio.h>

int my_print(int (*print)(const char*, ...)) {
  return print("Test\n");
}


#include "auto-seccomp-setup.h"
int main() {
// AUTO GENERATED
auto_setup_seccomp();

  my_print(&printf);
  return 0;
}
