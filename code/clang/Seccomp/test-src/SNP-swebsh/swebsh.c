#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define MAX_ARG_COUNT 11
/* #define DEBUG */

int running = 1;
int exit_code = 0;

char cwd[256];
/* char command[256]; */
char args[MAX_ARG_COUNT][256];
char buffer[256] __attribute__((aligned(4096)));
char* split_args[MAX_ARG_COUNT][MAX_ARG_COUNT] = { { NULL } };
int pipeNumber = 0;

void free_args()
{
    for(int i = 0; i < MAX_ARG_COUNT; i++)
    {
        for(int j = 0; j < MAX_ARG_COUNT; j++)
        {
            free(split_args[i][j]);
            split_args[i][j] = NULL;
        }
    }
}

void handle_redirection(char*** cmd_args)
{
    int index = 0;
    // iterate arguments looking for <, > and >>
    while((*cmd_args)[index] != NULL)
    {
        int input_redir = strcmp((*cmd_args)[index], "<");
        int output_redir = strcmp((*cmd_args)[index], ">");
        int output_redir_append = strcmp((*cmd_args)[index], ">>");
        if(input_redir == 0)
        {
            int in_fd = open((*cmd_args)[index+1], O_RDONLY);
            if(dup2(in_fd, STDIN_FILENO) == -1)
            {
                perror("SWEB shell: stdin");
                return;
            }
            if(close(in_fd) < 0)
            {
                perror("SWEB shell: close input");
                return;
            }
        }
        if((output_redir == 0) || (output_redir_append == 0))
        {
            int file_flags = O_WRONLY | O_CREAT;
            if(output_redir_append == 0)
                file_flags |= O_APPEND;
            if(output_redir == 0)
                file_flags |= O_TRUNC;
            fflush(stdout);
            int out_fd = open((*cmd_args)[index+1], file_flags, 0777);
            if(dup2(out_fd, STDOUT_FILENO) == -1)
            {
                perror("SWEB shell: stdout");
                return;
            }
            if(close(out_fd) < 0)
            {
                perror("SWEB shell: close output");
                return;
            }
        }
        // free (<|>|>>) <file_name>
        if((output_redir == 0) || (output_redir_append == 0) || (input_redir == 0))
        {
            free((*cmd_args)[index]);
            (*cmd_args)[index] = NULL;
            free((*cmd_args)[index+1]);
            (*cmd_args)[index+1] = NULL;
            index++;
        }
        index++;
    }
}

// ============================================================
// spawn_process
// ============================================================
int spawn_process(int in, int out, char* cmd_args[])
{
    pid_t pid;
    if((pid = fork()) == 0)
    {
        // ===============================================
        // == CHILD PROCESS ==
        // ===============================================
        if(in != STDIN_FILENO)
        {
            if(dup2(in, STDIN_FILENO) < 0)
            {
                perror("SWEB shell: stdin file desc");
                return EXIT_FAILURE;
            }
            if(close(in) < 0)
            {
                perror("SWEB shell: stdin close");
                return EXIT_FAILURE;
            }
        }
        if(out != STDOUT_FILENO)
        {
            if(dup2(out, STDOUT_FILENO) < 0)
            {
                perror("SWEB shell: stdout file desc");
                return EXIT_FAILURE;
            }
            if(close(out) < 0)
            {
                perror("SWEB shell: stdout close");
                return EXIT_FAILURE;
            }
        }
        handle_redirection(&cmd_args);
        // ===============================================
        // == EXEC CALL ==
        // ===============================================
        execvp(cmd_args[0], cmd_args);
        perror("SWEB Shell");
        exit(EXIT_FAILURE);
    }
    else if(pid < 0)
    {
        perror("Fork Error");
        return EXIT_FAILURE;
    }
    else
    {
        if(in != STDIN_FILENO)
        {
            if(close(in) < 0)
                perror("Parent Close In");
        }
        if(out != STDOUT_FILENO)
        {
            if(close(out) < 0)
                perror("Parent Close Out");
        }
    }
    return pid;
}

void cd(char* new_dir)
{
    printf("%s\n", new_dir);
}

void handle_command(char* buffer, int buffer_size)
{
    int c = 0;
    int argsCount = 0;
    int lastIndex = 0;
    // determine if current position (space) is surrounded by quotes
    // quotes cannot be part of the argument vector
    int insideQuotes = 0, lastArgumentQuotes = 0;
    int offset = 0;
    // if not inside Argument, skip spaces
    int insideArgument = 0;

    for (c = 0; c < buffer_size && buffer[c]; c++)
    {
        if (argsCount > MAX_ARG_COUNT)
        {
            argsCount = MAX_ARG_COUNT;
            printf("Argument Count is limited to 10 (no dynamic memory allocation) all other arguments will be ignored\n");
            break;
        }
        if(buffer[c] == '"')
        {
            insideQuotes ^= 1;
            if(!insideQuotes)
                lastArgumentQuotes = 1;
        }
        if(!isspace(buffer[c]) && !insideArgument)
        {
            insideArgument = 1;
            lastIndex = c;
        }

        if ((buffer[c] == '\r' || buffer[c] == '\n' || buffer[c] == ' ') 
                && !insideQuotes && insideArgument)
        {
            if(lastArgumentQuotes)
            {
                lastArgumentQuotes = 0;
                offset = 1;
            }
            memcpy(args[argsCount], buffer + lastIndex + offset, c - lastIndex - 2 * offset);
            args[argsCount][c - lastIndex] = 0;
            argsCount++;
            lastIndex = c + 1;
            offset = 0;
            insideArgument = 0;
        }
    }
    // ===============================================
    // SPLIT PIPES
    // ===============================================
    int split_cnt = 0, split_args_cnt = 0;
    for(int i = 0; i < argsCount; i++)
    {
        if(strcmp(args[i], "|") == 0)
        {
            pipeNumber++;
            split_cnt++;
            split_args_cnt = 0;
            continue;
        }
        split_args[split_cnt][split_args_cnt] = (char*) malloc((sizeof(char) 
                    * strlen(args[i])) + 1);
        strcpy(split_args[split_cnt][split_args_cnt], args[i]);
        split_args[split_cnt][split_args_cnt][strlen(args[i])] = 0; 
        split_args_cnt++;
    }
#ifdef DEBUG
    for(int i = 0; i < MAX_ARG_COUNT; i++)
    {
        for(int j = 0; j < MAX_ARG_COUNT; j++)
        {
            printf("%s\t", split_args[i][j]);
        }
        printf("\n");
    }
#endif 
    // ===============================================
    // HANDLE COMMANDS / BUILT-INS / CALLS
    // ===============================================
    int in = STDIN_FILENO, out = STDOUT_FILENO;
    if (strcmp(args[0], "exit") == 0)
    {
        c = 4;
        while (buffer[c] == ' ')
            c++;
        exit_code = atoi(&buffer[c]);
        printf("Exiting Shell with exit_code %d\n", exit_code);
        running = 0;
    }
    // shell builtins
    else if (strcmp(args[0], "cd") == 0)
    {
        cd(args[1]);
    }
    // ===============================================
    // FORK & EXEC
    // ===============================================
    else if (strcmp(args[0], "") != 0)
    {
        int pipes[pipeNumber * 2];
        int status;
        int pipeCount = 0;
        for(int i = 0; i <= pipeNumber; i++)
        {
            if(pipeNumber > 0)
            {
                if(pipe(pipes + pipeCount) < 0)
                {
                    perror("Pipe Error");
                    return;
                }
                out = pipes[pipeCount + 1];
            }
            if(i == pipeNumber)
                out = STDOUT_FILENO;
            // SPAWN PROCESS
            spawn_process(in, out, split_args[i]);
            // ===============================================
            // == PARENT PROCESS == 
            // ===============================================
            wait(&status);
            if(pipeNumber > 0)
                in = pipes[pipeCount];
            pipeCount+=2;
        }
        /* int returnVal = WEXITSTATUS(status); */
    }
}

int main(int argc, char *argv[])
{
    cwd[0] = '/';

    printf("\n\%s\n", "SWEB-Pseudo-Shell starting...\n");
    do
    {
        printf("\n%s %s%s", "SWEB:", cwd, "> ");
        fgets(buffer, 255, stdin);
        if(feof(stdin))
            break;
        buffer[255] = 0;
        handle_command(buffer, 256);
        for (size_t a = 0; a < 256; a++)
            buffer[a] = 0;
        // reset args array
        for (int i = 0; i < MAX_ARG_COUNT; i++)
            args[i][0] = '\0'; 
        free_args();
        pipeNumber = 0;
    } while (running);

    return exit_code;
}
