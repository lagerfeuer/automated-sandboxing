Working directory for all your source code.

## Possible Tests
* [Overview](https://stackoverflow.com/questions/4583746/what-open-source-c-projects-are-worth-studying-for-learning-good-design#4584284)
* http://www.gnu.org/software/gsl/
* http://www.kylheku.com/~kaz/kazlib.html
* https://www.gnu.org/software/binutils/
* https://www.gnu.org/software/coreutils/coreutils.html
* **Ncurses:**
  * https://github.com/jarun/nnn
  * https://github.com/brenns10/tetris
* https://github.com/antirez/redis
* https://github.com/curl/curl
* https://github.com/TheAlgorithms/C
* https://github.com/stedolan/jq
* https://github.com/lastpass/lastpass-cli
* https://github.com/tanakh/cmdline
* [More...](https://github.com/search?l=C&q=command+line&type=Repositories)