CC=clang
CXX=clang++
CXXFLAGS=-std=c++14  -fPIC
LDFLAGS=-shared
LIBS=-lclang

TARGET=plugin

HEADERS=-isystem /usr/include/llvm/
LIB_DIR=-L/usr/lib
