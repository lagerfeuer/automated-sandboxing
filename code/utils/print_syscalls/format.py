import sys

in_file = sys.argv[1]
out_file = sys.argv[2]

lines = []
with open(in_file, 'r') as f:
    for line in f:
        lines.append(line.replace('\n',''))

with open(out_file, 'a') as f:
    f.write(',\n'.join(lines) + '\n')
