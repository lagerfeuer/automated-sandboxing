#!/bin/bash

URL='https://raw.githubusercontent.com/torvalds/linux/master/arch/x86/entry/syscalls/syscall_64.tbl'
SRC=syscalls.h
TMP=$(mktemp)
LIST=$(mktemp)

curl "$URL" -o "$TMP"
###############################################################################################
# START
echo "#include <string>" > $SRC
echo >> $SRC
echo "const std::string syscall_names[] = {" >> $SRC

# generate list
grep -E -v "^#" "$TMP" | cut -f3 | sort -u | (
while read -r name; do
  if [[ $name ]]; then
    echo "\"$name\"" >> $LIST
  fi
done
)

# format
python format.py "$LIST" "$SRC"

###############################################################################################
# END
echo "};" >> $SRC
# echo "const std::set SYSCALLS(syscall_names);" >> $SRC

