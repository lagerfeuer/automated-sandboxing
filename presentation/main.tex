\documentclass[10pt,usenames,dvipsnames]{beamer}

\usetheme{metropolis}

% ===== PACKAGES =====
\usepackage{tikz}
\usetikzlibrary{shapes,shapes.multipart,shapes.misc,arrows,trees,positioning,backgrounds,fit}
% Regular
\tikzstyle{block} = [rectangle, draw, line width=.5mm,
text width=7em, text centered, rounded corners, minimum height=4em]
\tikzstyle{dark-block} = [rectangle, fill, text=white, line width=.5mm,
text width=7em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex', very thick, ->, line width=1mm]
% AST
\tikzstyle{ast-block} = [rectangle, draw, line width=.25mm,
text width=5em, text centered, rounded corners, minimum height=3em]
\tikzstyle{dark-ast-block} = [rectangle, fill, text=white, line width=.25mm,
text width=5em, text centered, rounded corners, minimum height=3em]
\tikzstyle{ast-line} = [draw, -latex', ->, line width=.5mm]
% tool tree
\tikzstyle{tree} = [rectangle, fill, text=white, rounded corners, anchor=west,
align=center, minimum width=8em, minimum height=1.5em]
% function decl tree
\tikzstyle{fd-tree} = [rectangle, fill, text=white, rounded corners, anchor=west,
align=center, minimum width=10em, minimum height=1.5em]
% flow chart
\tikzstyle{basic} = [rectangle, draw, line width=.3mm,
text width=7em, text centered, rounded corners, minimum height=4em]
\tikzstyle{decision} = [diamond, draw, line width=.3mm, text width=5em, text
centered]
\tikzstyle{special} = [circle, draw, line width=.3mm, text width=3em, text
centered]
\tikzstyle{flow} = [draw, -latex', thick, ->]
\tikzstyle{yes} = [draw, -latex', thick, ->, color=ForestGreen]
\tikzstyle{no} = [draw, -latex', thick, ->, color=red]
\usepackage{appendixnumberbeamer}
\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
\usepackage{xspace}
\usepackage{xcolor}
\colorlet{hlcolor}{Yellow!30}
\usepackage{minted}
\usemintedstyle{trac}
\usepackage{hyperref}
\usepackage{fancyvrb}
\usepackage{alltt}
\usepackage{multirow}
\usepackage[nodayofweek]{datetime}

% ===== COMMANDS =====
\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}
% = AST =
\renewcommand{\ttdefault}{txtt}
\newcommand{\decl}[1]{\textbf{\color{Green}#1}}
\newcommand{\expr}[1]{\textbf{\color{Purple}#1}}
\newcommand{\hl}[1]{\textcolor{Orange}{#1}} % hex or line number
\newcommand{\func}[1]{\textbf{\color{Cerulean}#1}}
\newcommand{\type}[1]{\textcolor{ForestGreen}{#1}}
\newcommand{\cast}[1]{\textcolor{RubineRed}{#1}}
\newcommand{\lval}[1]{\textcolor{Blue}{#1}}
% font size
\newcommand{\presentationsize}{\fontsize{.2cm}{.22cm}\selectfont}

% ===== TITLE =====
\title{Automated Sandboxing}
\subtitle{\ldots using the Clang compiler framework}
\newdate{date}{06}{11}{2018}
\date{\displaydate{date}}
\author{\large Lukas Deutz}
\institute{Institute for Applied Information Processing and Communications (IAIK)}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

% ===== PRESENTATION =====
\begin{document}

\maketitle

\metroset{sectionpage=none}
\section{Introduction}

% ===== INTRODUCTION =====
\begin{frame}[fragile]
  \frametitle{Introduction}
  \begin{block}{Overview}
    \begin{enumerate}
      % \item Why sandboxing?
    \item Seccomp
    \item Clang/LLVM
    \item \texttt{auto-seccomp} tool
      \begin{itemize}
      \item Design
      \item Implementation
      \item Results
      \item Future Work
      \end{itemize}
    \end{enumerate}
  \end{block}
\end{frame}

\iffalse
\begin{frame}[fragile]
  \frametitle{Why sandboxing?}
  \begin{itemize}
  \item restrict applications
  \item isolate
    \begin{itemize}
    \item different applications from each other
    \item application from the system
    \end{itemize}
  \item shield user from
    \begin{itemize}
    \item malicious (untrusted) code
    \item severe bugs
    \item attackers
    \end{itemize}
  \end{itemize}
\end{frame}
\fi

% ===== SECCOMP =====
\metroset{sectionpage=progressbar}
\section{Seccomp}

\begin{frame}[fragile]
  \frametitle{Seccomp}
  \begin{block}{What is seccomp?}
    \vspace{1em}
    \begin{itemize}
    \item Seccomp = \emph{Secure Computing}
    \item sandboxing for Linux applications
      \begin{itemize}
      \item define filters in source code
      \item block syscalls that are not in the filter set
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Seccomp -- Example}
  \inputminted[linenos]{c}{code/seccomp-basic.c}
\end{frame}
% \begin{frame}[fragile]
%   \frametitle{Seccomp -- Example}
%   \inputminted[linenos,highlightcolor=hlcolor,highlightlines={1-2}]{c}{code/seccomp-basic.c}
% \end{frame}
\begin{frame}[fragile]
  \frametitle{Seccomp -- Example}
  \inputminted[linenos,highlightcolor=hlcolor,highlightlines={3-7}]{c}{code/seccomp-basic.c}
  \pause
  \textbf{Can be a lot of work... $\rightarrow$ Let's automate it!}
\end{frame}
% \begin{frame}[fragile]
%   \frametitle{Seccomp -- Example}
%   \inputminted[linenos,highlightcolor=hlcolor,highlightlines={8}]{c}{code/seccomp-basic.c}
% \end{frame}
% \begin{frame}[fragile]
%   \frametitle{Seccomp -- Example}
%   \inputminted[linenos,highlightcolor=hlcolor,highlightlines={7-8}]{c}{code/seccomp-args.c}
% \end{frame}
% \begin{frame}[fragile]
%   \frametitle{Seccomp -- Example}
%   \inputminted[linenos]{c}{code/seccomp-args.c}
%   \textbf{Can be a lot of work... $\rightarrow$ Let's automate it!}
% \end{frame}

% ===== CLANG/LLVM =====
\section{Clang/LLVM}

\begin{frame}[fragile]
  \frametitle{Clang/LLVM}
  \begin{block}{LLVM}
    \vspace{.5em}
    Compiler infrastructure supporting many languages (\alert{front ends}) and
    architectures (\alert{back ends}).
  \end{block}
  \begin{center}
    \input{tikz/llvm.tex}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Clang/LLVM}
  \begin{block}{Clang}
    \begin{itemize}
    \item Most popular LLVM front end \\ $\rightarrow$ C/C++/Objective-C compiler
      % \item LLVM stack: compiler + debugger (\texttt{lldb}) + linker (\texttt{lld})
    \item Collection of source code analysis tools
    \item \textbf{Clang Plugin/Tooling Framework}
      \begin{itemize}
      \item Analysis
      \item Refactoring
      \item Break build
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Clang -- Plugins VS Tools}
  \begin{columns}[T]
    \column{.49\textwidth}
    \begin{block}{Plugin}
      \vspace{.3em}
      \begin{itemize}
      \item<2-> bound to the build system
      \item<3-> new invocation for every file
      \item<4-> custom warnings/errors
      \end{itemize}
    \end{block}
    \column{.49\textwidth}
    \begin{block}{LibTooling}
      \vspace{.3em}
      \begin{itemize}
      \item<2-> standalone tool
      \item<3-> keep information across files
      \item<4-> code refactoring/analysis
      \item<4-> \textit{e.g.} \texttt{clang-check, clang-format}
      \end{itemize}
    \end{block}
  \end{columns}
  % \only<5->{\textbf{LibClang} bindings for C/Python with higher level abstractions of the
  % underlying source code}
\end{frame}

\subsection{Abstract Syntax Tree}

\begin{frame}
  \frametitle{Abstract Syntax Tree}
  \input{tikz/lexer-parser-ast.tex}
\end{frame}

\begin{frame}
  \frametitle{Abstract Syntax Tree}
  \inputminted{c}{code/ast-main.c}
  \pause
  \textbf{Print AST:} \texttt{clang -Xclang -ast-dump -fsyntax-only ast.c}
\end{frame}

\begin{frame}
  \frametitle{Abstract Syntax Tree}
  \begin{center}
    % \scriptsize
    \fontsize{.2cm}{.25cm}\selectfont
\begin{alltt}
    \input{misc/ast-main.txt}
\end{alltt}
  \end{center}
  \normalsize
\end{frame}

\begin{frame}
  \frametitle{Abstract Syntax Tree}
  \begin{columns}
    \begin{column}{.4\textwidth}
      \inputminted{c}{code/ast-main.c}
    \end{column}
    \begin{column}{.6\textwidth}
      \presentationsize
      \begin{center}
        \input{tikz/ast-main.tex}
      \end{center}
      \normalsize
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Abstract Syntax Tree}
  \begin{columns}
    \begin{column}{.4\textwidth}
      \inputminted[highlightcolor=hlcolor,highlightlines={6}]{c}{code/ast-main.c}
    \end{column}
    \begin{column}{.6\textwidth}
      \presentationsize
      \begin{center}
        \input{tikz/ast-main-1.tex}
      \end{center}
      \normalsize
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Abstract Syntax Tree -- \texttt{FunctionDecl}}
  \begin{center}
    \input{tikz/functiondecl-docu.tex}
  \end{center}
\end{frame}

\iffalse
\begin{frame}
  \frametitle{Abstract Syntax Tree}
  \begin{columns}
    \begin{column}{.38\textwidth}
      \inputminted[highlightcolor=hlcolor,highlightlines={4}]{c}{code/ast.c}
    \end{column}
    \begin{column}{.6\textwidth}
      \presentationsize
      \begin{center}
        \input{tikz/ast-2.tex}
      \end{center}
      \normalsize
    \end{column}
  \end{columns}
\end{frame}
\fi


\begin{frame}
  \frametitle{Traversing the AST -- Visitor/Walker}
  \begin{center}
    \input{tikz/libtooling.tex}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Traversing the AST -- Visitor/Walker}
  \begin{center}
    \input{tikz/libtooling-color.tex}
  \end{center}
  \scriptsize
  \inputminted{cpp}{code/tool.cpp}
  \normalsize
\end{frame}

% ===== auto-seccomp TOOL =====
\section{\texttt{auto-seccomp} tool}
\subsection{Design}
\begin{frame}
  \frametitle{Design}
  \begin{block}{Idea}
    \begin{itemize}
    \item traverse AST and remember used syscalls
      \begin{itemize}
      \item in source code
      \item by \texttt{libc}
      \end{itemize}
    \item generate filters based on used syscalls
      \begin{itemize}
      \item \texttt{auto-seccomp-setup.h}
      \item \texttt{auto-seccomp-setup.c}
      \end{itemize}
    \item refactor \texttt{main} (if necessary)
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Implementation}
\begin{frame}
  \frametitle{Implementation}
  \begin{block}{Implementation}
    \begin{itemize}
    \item \textbf{syscalls}: \texttt{syscall\_64.tbl}
    \item \textbf{libc}: mapping of: libc function $\rightarrow$ used syscalls
    \item \textbf{main file} (if not already present):
      \begin{itemize}
      \item include \texttt{auto-seccomp-setup.h}
      \item call \texttt{auto\_setup\_seccomp()}
      \end{itemize}
    \item \textbf{\texttt{auto-seccomp-setup.c}}: contains seccomp rules
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Implementation}
  \begin{block}{Requirements}
    \begin{itemize}
    \item \textbf{compile\_commands.json}:
      \begin{itemize}
        % \item can be automated by the build system
      \item \texttt{make}: Build EAR\footnote{\url{https://github.com/rizsotto/Bear}}\\
        $\rightarrow$ \texttt{bear make}
      \item \texttt{cmake}: built-in\\
        $\rightarrow$ \texttt{cmake -DCMAKE\_EXPORT\_COMPILE\_COMMANDS=ON .}
      \end{itemize}
    \item \textbf{Clang/LLVM}: Version 7.0.0
    \item link \texttt{libseccomp}
    \end{itemize}
  \end{block}
\end{frame}

% \begin{frame}
%   \frametitle{Implementation -- Syscall Recognition}
%   \begin{center}
%     \input{tikz/flow-chart.tex}
%   \end{center}
% \end{frame}

\subsection{Results}
\begin{frame}
  \frametitle{Results -- Refactoring}
  \small
  \begin{columns}
    \begin{column}{.5\textwidth}
      \inputminted{c}{code/auto-seccomp-main-before.c}
    \end{column}
    \pause
    \begin{column}{.5\textwidth}
      \inputminted[highlightcolor=hlcolor,highlightlines={2,7}]{c}{code/auto-seccomp-main-after.c}
    \end{column}
  \end{columns}
  \normalsize
\end{frame}

\begin{frame}
  \frametitle{Results -- Generated Filters}
  \scriptsize
  \inputminted{c}{code/auto-seccomp-setup.c}
  \normalsize
\end{frame}

\begin{frame}
  \frametitle{Results -- Workflow}
  \begin{block}{New Workflow}
    \begin{enumerate}
    \item generate \texttt{compile\_commands.json}
    \item run tool
    \item compile
    \end{enumerate}
    $\rightarrow$ protected by automatically generated seccomp rules
  \end{block}
\end{frame}

\iffalse
\begin{frame}
  \frametitle{Results -- Performance}
  \begin{center}
    \begin{tabular}{|r|r|r|}\hline
      \multicolumn{2}{|c|}{\textbf{Source Code}} & \multirow{2}{*}{\textbf{Time (s)}} \\\cline{1-2}
      \textbf{Lines} & \textbf{Files} & \\\hline
      1100 & 1  & 0.06 \\
      600  & 10 & 0.11 \\\hline
    \end{tabular}
    \blfootnote{Intel Core i7-8550U}
  \end{center}
\end{frame}
\fi

\section{Future Work}
\begin{frame}
  \frametitle{Future Work}
  \begin{block}{Improvements}
    \begin{itemize}
    \item needs extensive testing
    \item support more libraries (\emph{e.g.,} ncurses)
      \begin{itemize}
      \item modular approach to libraries (command line option)
      \end{itemize}
    \item support argument filters
    \item custom \emph{setup points}
    \end{itemize}
  \end{block}
\end{frame}

% ===== END =====
\begin{frame}[standout]
  \Huge
  Questions?
\end{frame}

% \begin{frame}[allowframebreaks]
%   \frametitle{References}
%   \bibliography{references}
%   \bibliographystyle{abbrv}
% \end{frame}

\end{document}
