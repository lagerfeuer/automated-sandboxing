class MyVisitor : public clang::RecursiveASTVisitor<MyVisitor> {
  bool VisitFunctionDecl(clang::FunctionDecl* FD) { return true; }
  bool VisitCallExpr(clang::CallExpr* CE) { return true; }
};
