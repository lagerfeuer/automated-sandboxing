#include <stdio.h>
#include "auto-seccomp-setup.h"

int main(int argc, char* argv[])
{
  // AUTO GENERATED
  auto_setup_seccomp();
  // ...
  return 0;
}
