# Project:           Automated Sandboxing

**Student**:         Lukas Deutz

**Advisor**:         Daniel Gruß, Michael Schwarz

**Project goals**:   Automate Sandboxing with Seccomp filters (Clang Plugin)

**Project status**:  **Done**


## Documentation

Give a concise description of what your tool/code/design/work achieves.

If you did not include your actual implementation in the repository because you
contributed to an external project or used your own repo, document where to find it - 
branch name, repository URL, project's name, advisor's name or similar).

If you included your code, clearly document how to compile, set up, run, use your project.
(Preferably use the ["Markdown" (MD) markup language](https://help.github.com/articles/markdown-basics/))

After your project is finished and before the repository gets archived, make sure the
`master` branch indicates where to find all your relevant resources:

- Your thesis
- Your source code contributions
- Your presentations and posters
- Any other interesting material (websites, papers, ...)


## Status
**DONE**

### TODO
- [x] Play with Seccomp
- [x] Play with Clang (Plugin System) - e.g., Hello World Plugin
- [x] Generate list of used syscalls (direct and libc)
- [x] Generate Seccomp Rules
- [x] Add Seccomp Rules to build chain

