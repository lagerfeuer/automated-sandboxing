# Resources
## Syscalls
* [Anatomy of a Syscall](https://lwn.net/Articles/604287/)
* List of Syscalls in the Linux kernel under `arch/x86/entry/syscalls/syscall_{32,64}.tbl`

## Manuals/Docs
* [LLVM Programmer Manual](https://llvm.org/docs/ProgrammersManual.html)
* [Clang Internals Manual](http://clang.llvm.org/docs/InternalsManual.html)
* [Clang Plugins](http://clang.llvm.org/docs/ClangPlugins.html)

## Clang

### Using the clang command line
Using `-fplugin=plugin` on the clang command line passes the plugin through as an argument to `-load` on the cc1 command line.
If the plugin class implements the getActionType method then the plugin is run automatically.
For example, to run the plugin automatically after the main AST action (i.e. the same as using -add-plugin):

```cpp
// Automatically run the plugin after the main AST action
PluginASTAction::ActionType getActionType() override {
  return AddAfterMainAction;
}
```

### Interesting Clang Plugin
[clang-expand](https://github.com/goldsborough/clang-expand)

### Building Tools/Plugins with CMake
Look for `FindClang.cmake` and `FindLLVM.cmake` on Github! Then link the libraries accordingly.

### Clang AST
* **!!!** [Understanding the Clang AST](https://jonasdevlieghere.com/understanding-the-clang-ast/)
* [Clang AST Introduction](http://clang.llvm.org/docs/IntroductionToTheClangAST.html)
* [Clang AST Video](https://www.youtube.com/watch?time_continue=56&v=VqCkCDFLSsc) {
[Slides](http://llvm.org/devmtg/2013-04/klimek-slides.pdf)
}

### Clang Tooling
* **!!!** [clang-useful (video)](https://www.youtube.com/watch?v=E6i8jmiy8MY)
* **!!!** [clang-notes](https://github.com/peter-can-write/clang-notes)
* **Compilation Database**
  * [Compilation Database for Clang based tools](https://eli.thegreenplace.net/2014/05/21/compilation-databases-for-clang-based-tools/)
* [llvm-clang-samples (Github)](https://github.com/eliben/llvm-clang-samples)

### Clang Plugin (LibreOffice)
* **!!!** [How to write clang compiler plugins (LibreOffice)](https://www.youtube.com/watch?v=pdxlmM477KY)
* **!!!** [LibreOffice Clang Plugins](https://github.com/LibreOffice/core/tree/master/compilerplugins)
  `git clone https://github.com/LibreOffice/core.git && cd core && git revert bb71e3a40067e4ef6c6879e6d26ad20f728dd822`
* [Old, stored LibreOffice Clang Plugins (not in use anymore)](https://github.com/LibreOffice/core/tree/master/compilerplugins/clang/store)
* [Tutorial Plugins](https://github.com/LibreOffice/core/tree/master/compilerplugins/clang/store/tutorial)

### Clang Plugin cont'd
* [Tutorial Series (Part III)](https://kevinaboos.wordpress.com/2013/07/29/clang-tutorial-part-iii-plugin-example/)
* [Examples Github](https://github.com/lijiansong/clang-llvm-tutorial)
* [Constantine](https://github.com/rizsotto/Constantine)

## Seccomp
* Teach Seccomp
  * [Using Seccomp](http://outflux.net/teach-seccomp/) -> [Git Repo](https://github.com/gebi/teach-seccomp)
* [Introduction to Seccomp](https://blog.yadutaf.fr/2014/05/29/introduction-to-seccomp-bpf-linux-syscall-filter/)

# Additional Information

Optional: Your reading material for this project (papers, tool documentation, ...)

For well-formatted BibTeX references, check <https://dblp.org/>.
